[DJI Matrice 210]: https://www.dji.com/gr/matrice-200-series
[X5S]: https://www.dji.com/gr/zenmuse-x5s
[XT]: https://www.dji.com/gr/zenmuse-xt
[Tonbo's EOD 640]: http://tonboimaging.com/india/products-2/security-systems/eod640/
[SFEDA]: http://www.interreg-balkanmed.eu/approved-project/22/

# Information

Welcome to the repository containing my work on Theasis system, a novel forest fire monitoring system.  

Main programming languages are Javascript ES6 (NodeJS) for the network application, Java 8 for the Android application.    

HTTP/WebSocket/RTMP protocols have been integrated accordingly as per system's architecture.

Memcached and MongoDB are used in parallel to cache and store data.

To achieve full functionality a [DJI Matrice 210] with [X5S], [XT] cameras must be present.

Current document [gives an overview of system's main modules](#markdown-header-theasis-modules),  [information about dockerized build and deploy steps](#markdown-header-build/build-and-deployment), and  
[the software frameworks that are being used](#markdown-header-software-frameworks).  

Videos:

- [Presentation](https://www.youtube.com/watch?v=vKALFwkKzKI&feature=youtu.be)
- [System in action 1](https://www.youtube.com/watch?v=rkGmmHMgD-Y&feature=youtu.be)
- [System in action 2](https://www.youtube.com/watch?v=XIaIcv06y4c&feature=youtu.be)

- - -

# Table of contents

- [Theasis modules](#markdown-header-theasis-modules)
    - [Coordinator](#markdown-header-coordinator)
    - [Quadcopter control](#markdown-header-quadcopter-control)
    - [Image processing](#markdown-header-image-processing)
    - [Path planning](#markdown-header-path-planning)
    - [Video streaming](#markdown-header-video-streaming)
- [Build and deployment](#markdown-header-build-and-deployment)
    - [System's configuration](#markdown-header-configuration)
    - ["Dockerizing"](#markdown-header-docker-image)
        - [Information](#markdown-header-information)
        - [Procedure](#markdown-header-procedure)
    - [Quadcopter control application](#markdown-header-quadcopter-control-application)
- [Roadmap](#markdown-header-roadmap)
- [Software frameworks](#markdown-header-software-frameworks)
- [Contributors](#markdown-header-contributors)
- [Acknowledgments](#markdown-header-acknowledgments)
- [Licensing](#markdown-header-licensing)


- - -


## Coordinator
A NodeJS application which establishes 2 `WebSocket`, 1 `HTTP` servers, the connection to the database, and runs system's logic. 

If a fire alarm has been risen by image processing, an `HTTP GET` request is sent to coordinator HTTP server's `/new_alarm` URN, containing the estimated `latitude` and `longitude` as parameters.    
The coordinator initiates a fire assessment loop by checking if both the path planning and the quadcopter modules are connected -the latter must have strong GPS signal-.
If so, a route is requested between alarm's estimated location and quadcopter's current location, through `WebSocket` protocol.

Path planning module calculates the route and sends it back using `WebSocket` as well.  

Coordinator forwards the calculated path to the quadcopter's control application, which in return initiates an autonomous mission.

More details can be found in ```/theasis/coordinator/``` subfolder's README.md

## Quadcopter control
A Java application installed on an Android device which is connected through USB with DJI's Cendence remote controller (software-wise `DJI's Android SDK` is integrated).
This application expects a message from the coordinator (`WebSocket`) in case of a fire alarm. 

It is responsible for the initiation of quadcopter's autonomous missions, retransmission of quadcopter's telemetry (data and video feed) to the coordinator and media server, and every other function directly related with the quadcopter.
A more detailed README.md can be found in ```/theasis/navigation/``` subfolder.

## Image processing
A system's submodule developed by project's partners. Fire notifications and validations are created through HTTP requests.  

## Path planning
A system's submodule developed by project's partners. It calculates the path from quadcopter's landing position to estimated fire's coordinates which is later on transmitted to the quadcopter itself, to proceed with its autonomous mission.

## Video streaming
A docker image, pulled from https://github.com/JasonRivers/Docker-nginx-rtmp.  
An Nginx server with RTMP support. It additionally converts the streams to HLS, for HTTP interoperability. 


- - -
# Build and deployment
## Configuration
Coordinator can be configured to run its WebSocket/HTTP servers on different port, set a different quadcopter flying altitude on autonomous missions, 
use different keys' definitions when communicating with Memcached, etc, in `/theasis/modules/coordinator/src/systemParameters.js`.  

Quadcopter control application's functionality can also be parameterized in such senses, by changing values inside `/theasis/modules/navigation/androidApp/app/src/main/java/xanthopoulos/nikitas/theasis/Matrice210Parameters.java`

Changing the configuration of the media server, database and the WiFi network is explained in [Information](#markdown-header-information).

## Docker image 
### Information
Build process starts with pulling an ArchLinux docker image and ordering OS' package manager to install software frameworks (nodejs/python). The latter's setup tools are used to install Theasis' required libraries after the code base has been trasferred.

System needs to establish a WiFi network (the android device connected with DJI's Matrice 210 remote controller must be reachable), hence a WiFi card  supported by `hostapd` utility is attached. A tested driver is `nl80211`, define yours (and maybe an different name/SSID) in `/theasis/cicd/docker/configs/hostapd.conf`.
If you prefer different IP domain range for the WiFi network (default `10.0.0.2-10.0.0.50`), change the file  `/theasis/cicd/docker/configs/dnsmasq.conf` accordingly.

Video streams from Tonbo's EDO640 and DJI's Matrice 210 are pushed in a media server (modified Nginx). It's configuration can be found in `/theasis/cicd/docker/configs/nginx-rtmp.conf`

`/theasis/cicd/docker/dockerfiles/Dockerfile` contains instructions for building Theasis' image, while `/theasis/cicd/docker/docker-compose.yml` instructions on running it as a container in parallel with other containers (database/media server).

A variety of environment variables are expected in building step (`/theasis/cicd/docker/.env`). To create a WiFi access point, `WIFI_INTERFACE` must be correctly replaced (` ip link | grep wl ` or ` iw dev ` to discover yours in many Linux distributions).

### Procedure
Pull existing nginx-rtmp and memcached images from Docker's official repository:
```bash
docker pull jasonrivers/nginx-rtmp
docker pull memcached
```


Build Theasis' image using docker-compose `/theasis/cicd/docker/`:
```bash
docker-compose build
```


Create the containers from built images and run them using docker-compose `/theasis/cicd/docker/`:
```bash
docker-compose up
```

## Existing ArchLinux installation:
Run the installation script located in cicd/lscripts with root privileges to install required packages on the host machine `theasis/cicd/lscript/ `: 
```bash
chmod +x theasis_install.sh && chmod +x theasis.sh && sudo theasis_install.sh
```


Run the execution script located in the same folder, which also needs root priviledges (for WiFi network management) `/theasis/cicd/lscript/`:
```bash
./theasis.sh
```

## Quadcopter control application
The application has been developed and tested using Android Studio, SDK's and tools' versions are mentioned in the corresponding chapter of this document. 

`Gradle` takes care of building application's libraries, `navigation/androidApp/app/build.gradle/build.gradle` contains all the information about this process.  
`tech.gusavila92:java-android-websocket-client:1.2.1` library is used to implement WebSocket connection between the application and the coordinator.


Either use Android Studio to build and upload/install the application or do the following in `modules/navigation/androidApp/`, :    
```bash
gradle init
gradle build
adb install $path_to_apk
```

- - -  
# Software frameworks

> As explained in [Information](#markdown-header-information), a detailed list of the mandatory software can be extracted from `/theasis/cicd/docker/Dockerfile` (through build process, everything is installed via `pacman`, `npm`, `pip`, or is using a premade Docker image).  

Theasis is (notably) using:  
- `NodeJS` *11.14.0*  
- `Python` *3.7.3*    
- `Memcached` *1.5.12*	
- `Nginx`  
- `MongoDB` *4.0.4* (optional)	
- `Docker` *18.09.6*	
- `Android SDK` *28.0.0* / `Android build tools` *28.0.3*	
- `DJI Android SDK` *4.10*	

- - -  

# Acknowledgments

An Interreg Balkan Mediterranean 2014-2020 cooperation Programme funded project:

> [SFEDA] | Forest Monitoring System for Early Fire Detection and Assessment in the Balkan-Med Area.

![img](/logo_scaled.png)


- - -  

# Licensing
Developed software is released under GNU - General Public License v3.


