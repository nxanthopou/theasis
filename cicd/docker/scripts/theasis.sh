#!/bin/bash

# requires python3 with installed dependencies, node with installed dependencies, /create_wifi.sh script
/usr/bin/id -u theasis &>/dev/null || useradd theasis -m -p "$USER_PASS"
/usr/bin/echo root:"$ROOT_PASS" | chpasswd > /dev/null
/usr/bin/echo theasis:"$USER_PASS" | chpasswd > /dev/null

[ -d /$TARGET_DIR/logs ] && echo "" || /usr/bin/mkdir /$TARGET_DIR/logs
[ -d /$TARGET_DIR/pids ] && echo "" || /usr/bin/mkdir /$TARGET_DIR/pids
[ -x /$TARGET_DIR/create_wifi.sh ] && echo "" || chmod +x /$TARGET_DIR/create_wifi.sh

/usr/bin/echo "nameserver    ::1" > /"$TARGET_DIR"/resolv.conf
/usr/bin/echo "nameserver    127.0.0.1" >> /"$TARGET_DIR"/resolv.conf
/usr/bin/echo "nameserver    $WIFI_NETWORK_ENTRY" >> /"$TARGET_DIR"/resolv.conf

/usr/bin/echo "127.0.0.1 $CONTAINER_HOSTNAME" > /etc/hosts
/usr/bin/echo "::1 $CONTAINER_HOSTNAME" >> /etc/hosts
/usr/bin/echo "127.0.1.1 $CONTAINER_HOSTNAME.localdomain $CONTAINER_HOSTNAME" >> /etc/hosts
/usr/bin/echo "$WIFI_NETWORK_ENTRY $CONTAINER_HOSTNAME" >> /etc/hosts

/usr/bin/echo "ssid=$CONTAINER_HOSTNAME" >> /$TARGET_DIR/hostapd.conf


/usr/bin/echo "Initiating ssh server"
[ -f /etc/ssh/ssh_host_rsa_key ] && /usr/bin/echo "rsa key already exists" || /usr/bin/ssh-keygen -b 2048 -t rsa -f /etc/ssh/ssh_host_rsa_key -N "" > /dev/null 2>&1
[ -f /etc/ssh/ssh_host_dsa_key ] && /usr/bin/echo "dsa key already exists" || /usr/bin/ssh-keygen -b 1024 -t dsa -f /etc/ssh/ssh_host_dsa_key -N "" > /dev/null 2>&1
/usr/bin/sshd -p "$SSHD_PORT"

/usr/bin/echo "Initiating Theasis' functions"
/bin/bash /$TARGET_DIR/create_wifi.sh &
# websocket libray used by python interface spams stdout, so it is
# redirected to /dev/null on deployment
/usr/bin/su theasis -c "/usr/bin/python3 /$TARGET_DIR/$PATH_TARGET/$PATH_PLAN_ENTRY > /dev/null 2>&1 &"
/usr/bin/su theasis -c "/usr/bin/node /$TARGET_DIR/$NODE_TARGET/$NODE_ENTRY"
