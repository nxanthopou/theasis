#!/bin/bash

# ensure no other services interact with wifi interface (e.g. NetworkManager, such services randomize
# wifi cards' mac addresses for privacy reasons) and that it's the same with the container's creation one
# (this script is setting a default mac, using ip link)
/usr/bin/echo "WiFi Access Point: Killing existing dnsmasq/hostapd processes"
# killing dnsmasq/hostapd if they are running
[ ! -z "$(/usr/bin/pgrep dnsmasq)" ] && /usr/bin/kill "$(/usr/bin/pgrep dnsmasq)" || :
[ ! -z "$(/usr/bin/pgrep hostapd)" ] && /usr/bin/kill "$(/usr/bin/pgrep hostapd)" || :

# wireless interface configuration
/usr/bin/echo "WiFi Access Point: Configuring wireless interface.."
/usr/bin/rfkill block wlan && /usr/bin/rfkill unblock wlan && /usr/bin/sleep 1
/usr/bin/ip link set "$WIFI_INTERFACE" down && /usr/bin/sleep 1
/usr/bin/ip addr flush dev "$WIFI_INTERFACE" && /usr/bin/sleep 1
/usr/bin/ip link set "$WIFI_INTERFACE" address 02:01:02:04:01:08
/usr/bin/ip addr add "$WIFI_NETWORK_ENTRY"/"$WIFI_NETWORK_SUBNET_ADDRESS" brd + dev "$WIFI_INTERFACE"
/usr/bin/ip link set "$WIFI_INTERFACE" up && /usr/bin/sleep 1

# dhcp server

/usr/bin/sleep 1
/usr/bin/echo "WiFi Access Point: Initiating dnsmasq.."
/usr/bin/dnsmasq --interface="$WIFI_INTERFACE" \
    --listen-address=::1,127.0.0.1,"$WIFI_NETWORK_ENTRY" \
    --bind-dynamic \
    --dhcp-range="$WIFI_NETWORK_ENTRY","$WIFI_NETWORK_LAST_IP","$WIFI_NETWORK_SUBNET",12h \
    --dhcp-option-force=option:router,"$WIFI_NETWORK_ENTRY" \
    --dhcp-option-force=option:dns-server,"$WIFI_NETWORK_ENTRY" \
    --resolv-file=/$TARGET_DIR/resolv.conf \
    --log-queries \
    --log-facility=/$TARGET_DIR/logs/dnsmasq.log \
    --user=root \
    --no-hosts \
    -p 5353 #-d &
    

/usr/bin/echo "WiFi Access Point: Initiating hostapd.."
# access point software
/usr/bin/hostapd /$TARGET_DIR/hostapd.conf -i "$WIFI_INTERFACE" -B > /dev/null 2>&1

/usr/bin/sleep 2

# restart this script if dnsmasq/hostapd are not running
[ -z "$(/usr/bin/pgrep dnsmasq)" ] && /bin/bash /$TARGET_DIR/create_wifi.sh || /usr/bin/echo "WiFi Access Point: dnsmasq has successfully started"
[ -z "$(/usr/bin/pgrep hostapd)" ] && /bin/bash /$TARGET_DIR/create_wifi.sh || /usr/bin/echo "WiFi Access Point: hostapd has successfully started"

/usr/bin/echo "WiFi Access Point: WiFi's services have been initiated!"
