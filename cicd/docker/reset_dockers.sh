echo "stopping containers"
docker container stop $(docker container ls -a | awk 'NR>1{print $1}')
echo "deleting containers"
docker container rm $(docker container ls -a | awk 'NR>1{print $1}')
echo "deleting images"
docker image rm $(docker image ls -a | awk '/(<none>)|(theasis)/ {print $3}')
