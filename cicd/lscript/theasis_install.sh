#!/bin/bash
# ArchLinux
NODEDIR=$(readlink -f modules/main_server/theasis_api)

sudo pacman -Sy memcached nodejs npm create_ap dnsmasq
sudo pip install numpy mpu gmplot websocket
npm install --prefix $NODEDIR
npm audit fix --prefix $NODEDIR
