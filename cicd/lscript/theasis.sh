#!/bin/bash
# ArchLinux

NODEDIR=$(readlink -f modules/main_server/theasis_api)
NAVAPP=$(readlink -f modules/navigation/pathPlanning/motion_planner/websocket_client.py)
WIFI_IFACE=$(ip link | grep wl | awk '{print substr($2, 1, length($2)-1)}')

trap 'sudo kill $BGCREATEAP; kill $BGPYTHON; exit' SIGINT

sudo systemctl stop memcached
sudo systemctl start memcached
sudo systemctl start dnsmasq

sudo ip link set $WIFI_IFACE down
sudo create_ap -n $WIFI_IFACE theasis &> /dev/null &
BGCREATEAP=$!

python $NAVAPP &> /dev/null &
BGPYTHON=$!
node modules/main_server/theasis_api/src/theasisApp.js
