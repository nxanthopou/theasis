## Overview

Coordinator is built with **NodeJS**. 
In order to retain the minimum possible latency between coordinator's actions and a reasonable state-of-code, its related software has been split into submodules functionality-wise, is parameterized through a single file, and exploits NodeJS `event loop manager & asynchronous I/O` to the maximum. On-RAM database has been integrated to achieve read/write times of ~1ms. 
A combination of `HTTP` and `WebSocket` protocols are being used for fast and stable responses.

Detailed information about this module's architecture can be found in `docs/integration.pdf`.  

- - -  

## Build / Deployment

* Install required npm packages: `[modules/coordinator/]# npm install`
* Ensure memcached is correctly initiated and start the application: `[modules/coordinator/]# node theasisApp.js`
* Console output is colored to indicate serious errors (red), on-going and successful steps (yellow/green).


- - - 

## Software structure

- `theasisApp.js`: Application's entrypoint. It initializes coordinator's submodules.  
- `src/logicFunctions.js`: Theasis' high-level logic implementation. 
    - Messages from/to image processing, path planning are translated to steps as described in related document. 
    - Top-level checks (e.g. quadcopter's and path-planning's availability status) take place in-between these steps/messages.  
    - `processQueuedAlarm` method checks for existing alarms in database and starts an assessment loop if so.
    - `allModulesConnected` checks if there is at least one client connected on both WebSocket servers.
    - `checkPathPlanningStatus` method checks if path planning application is connected.
    - `checkMatriceAppStatus` method checks if quadcopter application is connected.
    - `checkQuadStatus` method checks if quadcopter has a valid GPS location.
    - `initiateMission` requests a route from path planning after above checks. It is later on forwarded to quadcopter through `src/pathPlanningConnection.js` and `src/matrice210Connection.js`. 
    - If anything fails, alarm is "queued", same checks take place after a predefined amount of time (`paramsCheckInterval` in `systemParamaters.js`).  
    * A successful flow is the following: `processQueuedAlarm` -> `allModulesConnected` -> `checkQuadStatus` -> `isQuadReady` -> `initiateMission`. Afterwards, WebSocket servers dedicated for path planning and quadcopter integrations take care of requesting a route and sending it to the quadcopter application.
    
- `src/httpServer.js`: Application's HTTP server. Entrypoints for manipulating some of system's data through `HTTP GET` requests are exposed through this server. 
    - `new_alarm`: Initializes a fire assessment loop. expects `latitude`, `longitude` as URL parameters.  
    - `validate_alarm`: Validates an alarm. Expects `latitude`, `longitude` as URL parameters.  
    - `quad_latest_data`: returns a JSON object with quad's latest message data.  
    - `quad_mission_data`: returns a JSON object (array) with quad's latest data over a small period.  
    - `delete_key`: deletes a key from the database. Expects `key` as parameter (`newAlarm` in case of deleting an alarm).  
- `src/matrice210Connection.js`: A WebSocket server dedicated to exchange information with quadcopter's control application and order it to start autonomous missions.  
    - `on-message` event: received datum from quadcopter application are sent to the database.  
    - `sendToQuad` static method is a wrapper that sends data to the quadcopter application. It is called when a route has been received from path planning in `pathPlanningConnection.js`.  
- `src/pathPlanningConnection.js`: A WebSocket server dedicated to exchange information with path planning.   
    - `on-message` event: received route from path planning is forward to the quadcopter application by calling mentioned `sendToQuad` method. Key is properly modified to represent an on-assessment alarm, not a new one.  
    - `requestNewRoute` static method is also a wrapper that sends a message to the path planning application when a new route is needed. Its input is a JSON object with `droneLatitude`, `droneLongitude`, `droneAltitude`, `alarmLatitude`, `alarmLongitude`, `targetAltitude` keys and their corresponding values and a nullable callback.  
- `src/memcachedFunctions.js`: A `memcached` library wrapper which sends/reads data from main database. Most notable methods/`memcached` library's wrappers are the following:  
    - `getKey` is memcached's `get` wrapper which expects a `string` and retrieves data from the database.
    - `setKey` is memcached's `set` wrapper which expects a `string` as a key, a `string` as value, an `int` as key's lifetime in seconds (0 for permanent) to store data in the database.
    - `deleteKey` is memcached's `delete` wrapper which expects a `string` to delete data from the database.  
    - `prependKey` is memcached's `prepend` wrapper which expects a `string` as a key, a `string` as a value to append, and adds data in front of a custom created array.  
- `src/systemParameters.js`: Coordinator's configuration, high-level system parameterization.  
    - Quadcopter's relative to take-off position flying altitude and flying speed.  
    - HTTP/WebSocket servers' listening ports.  
    - `memcachedListMaxSize` is the maximum array length that is allowed in memcached. If this limit is reached, data is dumped in a local file/MongoDB and the memcached key is reset.
    - `memcachedKeyLifetime` is the keys' lifetime in memcached (in seconds - 0 means keep "forever").  
    - `memcachedAlarmKey` is memcached's key used for alarms (value contains `latitude`, `longitude` of on-going alarms).
    - `memcachedQuadLatestDataKey` is memcached's key used for latest received data from quadcopter. Its value is a serialized JSON dictionary formed in `parseMessage()` containing key-value pairs like:
        - `toc`: time of creation .
        - `lat`: quadcopter's latitude.
        - `lon`: quadcopter's longitude.
        - `relAlt`: relative altitude (from take-off/home position - if any).
        - `absAlt`: absolute altitude of the take-off/home position from sea level.
        - `gps`: GPS signal level (enum `LEVEL_0` to `LEVEL_5`).
        - `batt`: quadcopter's remaining battery.
        - `rcBatt`: remote controller's remaining battery.
        - `roll`: quadcopter's roll.
        - `pitch`: quadcopter's pitch.
        - `gimR`: gimbal's roll.
        - `gimP`: gimbal's pitch.
        - `gimY`: gimbal's yaw.
        - More can added by configuring `parseMessage()` and android application's WebSocket data exposal (`Matrice210DataExposal.java`).
        * Quadcopters' data are received every X second(s) (defined in `Matrice210Parameters.java`,  variable `apiConnectionPeriod`, defaults to 1 second); once received, `memcachedQuadLatestDataKey` is reset to latest data.  
        * JSON dictionary's key names are set to a minimum character count to keep smallest possible footprint in RAM.  


### Parameterization
Edit `src/systemParameters.js` to change:

- Quadcopter's mission flight speed:  

```javascript
        module.exports.quadAutoFlightSpeed = 15.0; // meters per second
```

- Quadcopter's target altitude (relative to take-off/home position)

```javascript
        module.exports.quadTargetAltitude = 40.0; // meters
```

- The interval for high-level checks:

```javascript
        module.exports.paramsCheckInterval = 1000; // millis
```

- HTTP, WebSocket ports to listen, memcached's listening port:

```javascript
    module.exports.httpPort = 3000;
    module.exports.wsPathPlanningPort = 3001;
    module.exports.wsMatricePort = 3002;
    module.exports.memcachedPort = 11211;
    module.exports.memcachedHost = "localhost";
```

- Memcached configuration. The name of the local file to save mission data, the maximum allowed array length stored as string, the keys' lifetime, some key definitions and client's options:

```javascript
    module.exports.memcachedExternalPersistenceFileName = "quadData.log";
    module.exports.memcachedListMaxSize = 500;
    module.exports.memcachedKeyLifetime = 0;
    module.exports.memcachedAlarmKey = "newAlarm";
    module.exports.memcachedMissionAssessmentKey = "assessedMission";
    module.exports.memcachedRouteKey = "newRoute";
    module.exports.memcachedQuadLatestDataKey = "quadLatestData";
    module.exports.memcachedQuadMissionData = "quadMissionData";
    module.exports.memcachedCliOptions = {
                retries:1,
                retry:50,
                remove:true//,
                //failOverServers:['failSafeHostname:11211']
            }
```

- WebSocket configuration. Interval for ping-pong functionality with path planning and android application, maximum allowed pong time before assuming a client is disconnected abnormally after a ping request.

```javascript
    module.exports.webSocketClientCheckInterval = 1000;
    module.exports.webSocketClientIsOnlineTimeout = 5000;
```

- - -  

## Additional libraries
Additional packages were installed via **npm**:  

* `ws`: WebSocket server(s).  
* `express`: HTTP server.  
* `memcached`: A Memcached Javascript API.  
* `mongodb`: A MongoDB Javascript API.  
* `chalk`: Colored console output.  

- - -  

## Demo

![img](coord_success.png)

- - -  

## Notes

- `JSON` format is used to transmit data over `HTTP`/`WebSocket` protocols and store data in `memcached`.  
- `memcached` service must be successfully started before initializing coordinator. An appropriate message will be displayed in console if connection with database is broken.  
- `memcached` database is key-value and allows only one type of data, `string`. Hence, `JSON` objects are serialized before sent to the database and arrays are stored/retrieved in/from strings with custom seperators.  
- Coordinator's host machine and quadcopter application's host tablet must be in the same network. Ensure that access point has successfully started as per main page's documentation and coordinator's host machine IPs include `192.168.12.1`. If different, quadcopter application's configuration must be set appropriately (`matrice210Parameters.js`).  

