/*
Developer: Xanthopoulos Nikitas,
2019
*/

/********************
 * General          *
*********************/
module.exports.quadTargetAltitude = 40.0; // waypoints' relative altitude (from take off pos)
module.exports.quadAutoFlightSpeed = 15.0;
module.exports.paramsCheckInterval = 1000; // interval between checking if everything is ok before sending mission to UAV (android app)



/********************
 * Ports/Networking *
*********************/
module.exports.httpPort = 3000;
module.exports.wsPathPlanningPort = 3001;
module.exports.wsMatricePort = 3002;
module.exports.wsUIPort = 3003;
module.exports.memcachedPort = 11211;
module.exports.memcachedHost = "localhost";


/*****************
 * Memcached     *
******************/
module.exports.memcachedExternalPersistenceFileName = "quadData.log";
module.exports.memcachedListMaxSize = 500;
module.exports.memcachedKeyLifetime = 0;
module.exports.memcachedAlarmKey = "newAlarm";
module.exports.memcachedMissionAssessmentKey = "assessedMission";
module.exports.memcachedRouteKey = "newRoute";
module.exports.memcachedQuadLatestDataKey = "quadLatestData";
module.exports.memcachedQuadMissionData = "quadMissionData";
module.exports.memcachedCliOptions = {
            retries:1,
            retry:50,
            remove:true//,
            //failOverServers:['failSafeHostname:11211']
        }

/*****************
 * WebSocket     *
******************/
module.exports.webSocketClientCheckInterval = 1000; // ping/pong websocket server interval
module.exports.webSocketClientIsOnlineTimeout = 5000;
// websocket optimization, needs OS check for memory leaks on production (sth about zlib/nodejs on Linux)
module.exports.websocketOptimization = {
                zlibDeflateOptions: {
                    // See zlib defaults.
                    chunkSize: 1024,
                    memLevel: 7,
                    level: 3
                },
                zlibInflateOptions: {
                    chunkSize: 10 * 1024
                },
                // Other options settable:
                clientNoContextTakeover: true, // Defaults to negotiated value.
                serverNoContextTakeover: true, // Defaults to negotiated value.
                serverMaxWindowBits: 10, // Defaults to negotiated value.
                // Below options specified as default values.
                concurrencyLimit: 10, // Limits zlib concurrency for perf.
                threshold: 1024 // Size (in bytes) below which messages
            // should not be compressed.
            }
