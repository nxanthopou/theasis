/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/

const chalk = require('chalk');
const pathPlanningConnection = require("./pathPlanningConnection.js").PathPlanningConnector;
const matriceConnection = require("./matrice210Connection.js").MatriceConnector;
const memcachedFunctions = require("./memcachedFunctions.js").MemcachedClient;
const uiConnection = require("./uiConnection.js").UserInterfaceConnector;

const systemParams = require("./systemParameters.js");
const keyLifetime = systemParams.memcachedKeyLifetime;
const alarmKey = systemParams.memcachedAlarmKey;
const alarmAssessmentKey = systemParams.memcachedAlarmAssessmentKey;
const quadLatestDataKey = systemParams.memcachedQuadLatestDataKey;
const paramsCheckInterval = systemParams.paramsCheckInterval;
const targetAltitude = systemParams.quadTargetAltitude;

class LogicFunctions {
    static processQueuedAlarm() {
        memcachedFunctions.getKey(alarmKey, function (alarmData) {
            try {
                if (alarmData == undefined) return;
                let parsedAlarmData = memcachedFunctions.parseJSON(alarmData);
                if (LogicFunctions.allModulesConnected()) {
                    LogicFunctions.checkQuadStatus(parsedAlarmData);
                } else {
                    LogicFunctions.queueAssessment();
                }
             } catch (exc) {
                errLog(`${exc} rechecking in ${paramsCheckInterval} seconds`);
                LogicFunctions.queueAssessment();
             }
        });
    }

    static allModulesConnected() {
        let isPPOnline = LogicFunctions.checkPathPlanningStatus();
        let isMatriceAppOnline = LogicFunctions.checkMatriceAppStatus();
        if (isPPOnline == true && isMatriceAppOnline == true) return true;
        return false;
    }

    static checkQuadStatus(alarmData) {
        memcachedFunctions.getKey(quadLatestDataKey, function (quadData) {
            try {
                let parsedQuadData = memcachedFunctions.parseJSON(quadData);
                let quadReady = LogicFunctions.isQuadReady(parsedQuadData);
                if (quadReady) {
                    LogicFunctions.initiateMission(parsedQuadData, alarmData);
                } else {
                    throw "quad is not ready (low GPS signal..?)";
                }
            } catch (exc) {
                errLog(`${exc}, rechecking in ${paramsCheckInterval} seconds`);
                LogicFunctions.queueAssessment();
            }
        });
    }

    static initiateMission(parsedData, alarmData) {
        try {
            let missionPoints = {
                "droneLatitude" : parseFloat(parsedData.lat),
                "droneLongitude" : parseFloat(parsedData.lon),
                "droneAltitude" : parseFloat(parsedData.alt),
                "alarmLatitude" : parseFloat(alarmData.lat),
                "alarmLongitude" : parseFloat(alarmData.lon),
                "targetAltitude" : parseFloat(targetAltitude) + parseFloat(parsedData.toAlt)
                };
            pathPlanningConnection.requestNewRoute(missionPoints, function () {
                    console.log(chalk.yellow("requesting route from path planning.."));
                });
        
            // uiConnection.sendAlarm()
            // is called through HTTPServer module
        } catch (err) {
            errLog(`quadData err:  ${err}`);
            LogicFunctions.queueAssessment();
        }
    }

    static isQuadReady(parsedData) {
        // DJI returns 0.0 or -1.0 when GPS signal
        // is very weak.
        const validLat = (parseFloat(parsedData.lat) >= 1.0)? true : false;
        const validLong = (parseFloat(parsedData.lon) >= 1.0)? true : false;
/*
        const quadOnMission = parsedData.onMission == "true";

        const quadGPSSignalLevel = (parsedData.gps == 'LEVEL_5' || parsedData.gps == 'LEVEL_0')? true : false;
*/
        if (validLat && validLong) { //  || quadOnMission || quadGPSSignalLevel
            return true;
        }
        return false;
    }

    static checkPathPlanningStatus() {
        try {
            if (pathPlanningConnection.webSocketServer.clients.size > 0) return true;
            throw "path planning offline"
        } catch (exc) {
            console.log(chalk.red(`${exc}`));
            return false;
        }
    }

    static checkMatriceAppStatus() {
        try {
            if (matriceConnection.webSocketServer.clients.size > 0) return true;
            throw "matrice application offline"
        } catch (exc) {
            console.log(chalk.red(`${exc}`));
            return false;
        }
    }

    static queueAssessment() {
        setTimeout(function() {
            LogicFunctions.processQueuedAlarm();
        }, paramsCheckInterval);
    }
}

function errLog(logData) {
    console.log(chalk.red(logData));
}
module.exports.LogicFunctions = LogicFunctions;
