/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/
'use strict';
const WebSocket = require('ws');
const chalk = require('chalk');
const systemParams = require('./systemParameters.js');

const parMessageDeflateOpts = systemParams.websocketOptimization;
const wsCliCheckInterval = systemParams.webSocketClientCheckInterval;

class UserInterfaceConnector {
    static initWebSocketServer(port, callback) {
        const wss = new WebSocket.Server({
            port: port,
            perMessageDeflate: parMessageDeflateOpts
        });

        wss.on('connection', function connection(webSocket, req) {
            UserInterfaceConnector.isAlive = true;
            const interval = UserInterfaceConnector.checkAvailability(webSocket);

            // if behind nginx, client ip is found from headers['x-forwarded-for']
            const cliIP = (req.connection.remoteAddress)? req.connection.remoteAddress : req.headers['x-forwarded-for'].split(/\s*,\s*/)[0];

            webSocket.on('message', function incoming(message) {
                let parsedMessage = JSON.parse(message);

            });

            webSocket.on('close', function close() {
                clearInterval(interval);
            });
            console.log(chalk.underline(chalk.yellow(`Coordinator: A UI client has connected, ${cliIP}`)));
        });
        UserInterfaceConnector.webSocketServer = wss;
        callback();
    }

    static checkAvailability(webSocket) {
        const interval = setInterval(function ping() {
                (UserInterfaceConnector.webSocketServer.clients.size > 0)? UserInterfaceConnector.isAlive = true : UserInterfaceConnector.isAlive = false;
        }, wsCliCheckInterval);
        return interval;
    }

    static sendAlarm(alarmInfo) {
        let object = {"alarmCoordinates": {"latitude": alarmInfo.latitude, "longitude": alarmInfo.longitude}}
        UserInterfaceConnector.webSocketServer.clients.forEach(function each(clientWS) {
            clientWS.send(JSON.stringify(object));
        });
    }
}


module.exports.UserInterfaceConnector = UserInterfaceConnector;


