/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/
'use strict';
const chalk = require('chalk');
const Memcached = require('memcached');
const systemParams = require('./systemParameters.js');
const fs = require('fs');

const keyLifetime = systemParams.memcachedKeyLifetime;
const quadMissionDataKey = systemParams.memcachedQuadMissionData;
const quadLatestDataKey = systemParams.memcachedQuadLatestDataKey;
const maxListSize = systemParams.memcachedListMaxSize;
const fileName = systemParams.memcachedExternalPersistenceFileName

const dbHostname = systemParams.memcachedHost;
const dbPort = systemParams.memcachedPort;
const dbCliOptions = systemParams.memcachedCliOptions;

class MemcachedClient {
    static initMemcachedCli(host = dbHostname, port = dbPort, callback) {
        var memcachedClient = new Memcached(`${host}:${port}`, dbCliOptions);
        MemcachedClient.dbClient = memcachedClient;
        MemcachedClient.dbClient.stats(function (err, data) {
            if (err) console.log(chalk.red("Memcached was not initialized, start the service and restart the application"));
            else {
                MemcachedClient.initExternalPersistence();
                callback();
            }
        });
    }

    static initExternalPersistence() {
        MemcachedClient.initLogFile();
    }

    static initLogFile(){
        fs.writeFile('mission.log', '', function() {});
        fs.writeFile('quadData.log', '', function() {});
    }

    static isEmptyReply(value) {
        if (value === undefined) return true;
        return false;
    }

    static setDBKeys() {
        MemcachedClient.getKey(quadMissionDataKey, function(data) {
            if (MemcachedClient.isEmptyReply(data)) MemcachedClient.setKey(quadMissionDataKey, "", keyLifetime, function() {});
        });
        MemcachedClient.getKey(quadLatestDataKey, function(data) {
            if (MemcachedClient.isEmptyReply(data)) MemcachedClient.setKey(quadLatestDataKey, "{}", keyLifetime, function() {});
        });
    }

    static touchKey(key, lifetime, callback) {
        MemcachedClient.dbClient.touch(key, lifetime, function (err) {
            err? console.log(chalk.red(err)) : callback();
        });
    }

    static getKey(key, callback) {
        MemcachedClient.dbClient.get(key, function (err, data) {
            err? console.log(chalk.red(err)) : callback(data);
        });
    }

    static async getPromiseKey(key) {
        let promise = new Promise(function (resolve, reject) {
            let res = MemcachedClient.getKey(key, function (data) {
                data? resolve(data) : reject();
            });
        });
        return promise;
    }

    static setKey(key, value, lifetime, callback) {
        MemcachedClient.dbClient.set(key, value, lifetime, function (err) {
            err? console.log(chalk.red(err)) : callback();
        });
    }

    static deleteKey(key, callback) {
        MemcachedClient.dbClient.del(key, function (err) {
            err? console.log(chalk.red(err)) : callback();
        });
    }

    static serializeObject(JSONObject) {
        return JSON.stringify(JSONObject)
    }

    static parseJSON (serializedObject) {
        return JSON.parse(serializedObject)
    }

    // "[...]||{instance}||{instance}||"
    static deserializeData(dataString) {
        let dataArr = new Array();
        let numOfInstances = MemcachedClient.getCustomStrListSize(dataString);
        if (numOfInstances == 0) return [];
        for (let counter = 0; counter < numOfInstances; counter++) {
            dataArr.push(MemcachedClient.parseJSON(dataString.substring(0, dataString.indexOf("||"))));
            dataString = dataString.substring(dataString.indexOf("||")+2, dataString.length);
        }
        return dataArr;
    }

    static prependKey(key, value, callback) {
        const valueEdited = value + "||";
        MemcachedClient.dbClient.prepend(key, valueEdited, function (err) {
            if (err) {
                console.log(chalk.red(`error prepending key ${key}`));
            } else {
                callback();
            }
        });
    }

    static ensureListSize(key, callback) {
        MemcachedClient.getKey(key, function(data) {
            if (MemcachedClient.getCustomStrListSize(data) >= maxListSize) {
                MemcachedClient.missionDataToFile(key);
            }
            callback();
        });
    }

    static getCustomStrListSize(dataString) {
        let dataInstances = dataString.match(/\|\|/g);
        if (dataInstances === null) return 0;
        var numOfInstances = dataInstances.length;
        return numOfInstances;
    }

    static missionDataToFile(key) {
        MemcachedClient.getKey(key, function (data) {
            let deserializedData = MemcachedClient.deserializeData(data);
            if (deserializedData.length > 0) { // avoid concurrent calls from ensureListSize()
                fs.appendFile(fileName, `${JSON.stringify(deserializedData)},`, function (err) {
                    if (err) {
                        console.log(chalk.red(err));
                    } else {
                        MemcachedClient.setKey(key, "{}", keyLifetime, function() {});
                    }
                })
            };
        });
    }
}

module.exports.MemcachedClient = MemcachedClient;
