/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/

const WebSocket = require('ws');
const chalk = require('chalk');
const matriceConnection = require("./matrice210Connection.js").MatriceConnector;
const memcachedFunctions = require("./memcachedFunctions.js").MemcachedClient;
const systemParams = require('./systemParameters.js');

const keyLifetime = systemParams.memcachedKeyLifetime;
const routeKey = systemParams.memcachedRouteKey;
const alarmKey = systemParams.memcachedAlarmKey;
const parMessageDeflateOpts = systemParams.websocketOptimization;
const wsCliCheckInterval = systemParams.webSocketClientCheckInterval;
const missionAssessmentKey = systemParams.memcachedMissionAssessmentKey;

class PathPlanningConnector {
    static initWebSocketServer(port, callback) {
        const wss = new WebSocket.Server({
            port: port,
            perMessageDeflate: parMessageDeflateOpts
        });

        wss.on('connection', function connection(webSocket, req) {
            PathPlanningConnector.isAlive = true;
            const interval = PathPlanningConnector.checkAvailability(webSocket);
            const cliIP = (req.connection.remoteAddress)? req.connection.remoteAddress : req.headers['x-forwarded-for'].split(/\s*,\s*/)[0];
            console.log(chalk.underline(chalk.cyan(`Coordinator: Path planning connected, ${cliIP}`)));

            webSocket.on('message', function incoming(message) {
                let parsedMessage = JSON.parse(message);
                var assessmentInfo = {};
                console.log(chalk.yellow(`Coordinator: Path planning sent: ${JSON.stringify(parsedMessage, null, 3)}`));
                
                if (parsedMessage.route == null) return;
                assessmentInfo.route = parsedMessage.route;
                assessmentInfo.batts = [parsedMessage.remainingBattery, parsedMessage.remainingRCBattery];
                let assessmentInfoSerialized = JSON.stringify(assessmentInfo);

                memcachedFunctions.setKey(routeKey, JSON.stringify(parsedMessage.route), keyLifetime, function (err) {});
                memcachedFunctions.setKey(missionAssessmentKey, assessmentInfoSerialized, keyLifetime, function() {});
                matriceConnection.sendToQuad(parsedMessage, () => { console.log(chalk.bgGreen(`Coordinator: Route sent to quad app!`)); });
            });

            webSocket.on('close', function close() {
                clearInterval(interval);
                console.log(chalk.underline(chalk.red(`${new Date().toISOString()}: path planning disconnected`)));
            });
        });
        PathPlanningConnector.webSocketServer = wss;
        callback();
    }

    static checkAvailability(webSocket) {
        const interval = setInterval(function ping() {
                (PathPlanningConnector.webSocketServer.clients.size > 0)? PathPlanningConnector.isAlive = true : PathPlanningConnector.isAlive = false;
        }, wsCliCheckInterval);
        return interval;
    }

    static requestNewRoute(missionInfo, callback) {
        PathPlanningConnector.webSocketServer.clients.forEach(function each(clientWS) {
            clientWS.send(JSON.stringify(missionInfo));
        });
        callback();
    }
}


module.exports.PathPlanningConnector = PathPlanningConnector;


