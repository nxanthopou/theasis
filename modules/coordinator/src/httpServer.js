/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/
'use strict';
const express = require('express');
const chalk = require('chalk');

const memcachedFunctions = require("./memcachedFunctions.js").MemcachedClient;
const logicFunctions = require("./logicFunctions.js").LogicFunctions;
const uiConnection = require("./uiConnection.js").UserInterfaceConnector;
const systemParams = require("./systemParameters.js");


const keyLifetime = systemParams.memcachedKeyLifetime;
const alarmKey = systemParams.memcachedAlarmKey;
const quadLatestDataKey = systemParams.memcachedQuadLatestDataKey;
const quadMissionDataKey = systemParams.memcachedQuadMissionData;

class HttpServer {
    static initNewAlarmEP() {
        HttpServer.httpApp.get('/new_alarm',
            function (req, res) {
                try {
                    let alarm = {};
                    let alarmLatitude = req.query.latitude;
                    let alarmLongitude = req.query.longitude;
                    if (HttpServer.arrHasNull([alarmLatitude, alarmLongitude])) {
                        res.send("invalid lat/lon\n");
                        throw "null lat/long in /new_alarm"
                    }
                    alarm["lat"] = alarmLatitude;
                    alarm["lon"] = alarmLongitude;
                    alarm["toc"] = Date.now();
                    
                    const uiInfo = { "latitude": alarmLatitude, "longitude": alarmLongitude};
                    uiConnection.sendAlarm(uiInfo);
                    
                    const serializedAlarm = memcachedFunctions.serializeObject(alarm);
                    
                    memcachedFunctions.getKey(alarmKey, function(data) {
                        if (data == null) {
                            console.log(chalk.bgRed(`new alarm detected: ${alarmLatitude}, ${alarmLongitude}`));
                            memcachedFunctions.setKey(alarmKey, serializedAlarm, keyLifetime, function () {
                                // takes 1 more db read, to avoid almost duplicate code
                                // when logic is queuing alarms ( checking / creating )
                                logicFunctions.processQueuedAlarm();
                                res.send("alarm added\n");
                            });
                        } else {
                            res.send("alarm exists already, proceed with or delete it first\n");
                        }
                    });
                } catch (err) {
                    console.log(chalk.red(err));
                }
            }
        );
    }

    static initDeleteKeyEP() {
        HttpServer.httpApp.get('/delete_key',
            function (req, res) {
                try {
                memcachedFunctions.deleteKey(req.query.key, function (err) {
                    if (err) throw err
                    console.log(chalk.yellow("alarm removal requested"));
                    res.send("request handled\n");
                    });
                } catch (err) {
                    console.log(chalk.red(err));
                }
            }
        );
    }

    static initUnvalidatedAlarmsEP() {
        HttpServer.httpApp.get('/unvalidated_alarms',
            function (req, res) {
                try {
                    memcachedFunctions.getKey(alarmKey, function (data) {
                        (data)? res.send(data + "\n") : res.send("\n");
                    });
                } catch (err) {
                    console.log(chalk.red(err));
                }
            }
        );
    }

    static initQuadLatestDataEP() {
        HttpServer.httpApp.get('/quad_latest_data',
            function (req, res) {
                try {
                    memcachedFunctions.getKey(quadLatestDataKey, function (data) {
                        (data)? res.send(data + "\n") : res.send("\n");
                    });
                } catch (err) {
                    console.log(chalk.red(err));
                }
            }
        );
    }

    static initQuadMissionDataEP() {
        HttpServer.httpApp.get('/quad_mission_data',
            function (req, res) {
                try {
                    memcachedFunctions.getKey(quadMissionDataKey, function (data) {
                        if (data) {
                            let parsedData = memcachedFunctions.deserializeData(data);
                            res.send(parsedData);
                        } else {
                            res.send("\n");
                        }
                    });
                } catch (err) {
                    console.log(chalk.red(err));
                }
            }
        );
    }

    static initValidateAlarmEP(app) {
        HttpServer.httpApp.get('/validate_alarm',
            function (req, res) {
                console.log(chalk.bgRed(`Fire detected, latitude: ${req.query.latitude}\t longitude:${req.query.longitude}`))
                res.send("fire detection message reached main system")
                // manage email/sms/w.e
            }
        );
    }

    static initHTTPServer(httpPort, callback) {
        HttpServer.httpApp = express();
        HttpServer.initNewAlarmEP();
        HttpServer.initDeleteKeyEP();
        HttpServer.initUnvalidatedAlarmsEP();
        HttpServer.initQuadLatestDataEP();
        HttpServer.initQuadMissionDataEP();
        HttpServer.initValidateAlarmEP();
        HttpServer.httpApp.listen(httpPort, function () {
            callback();
        });
    }

    static arrHasNull(array) {
        for (let arrIndex in array) {
            if (array[arrIndex] == null) return true;
        }
        return false;
    }
}

module.exports.HttpServer = HttpServer;
