/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/
'use strict';
const WebSocket = require('ws');
const chalk = require('chalk');
const memcachedFunctions = require("./memcachedFunctions.js").MemcachedClient;
const systemParams = require('./systemParameters.js');

const port = systemParams.wsMatricePort;
const keyLifetime = systemParams.memcachedKeyLifetime;
const quadLatestDataKey = systemParams.memcachedQuadLatestDataKey;
const parMessageDeflateOpts = systemParams.websocketOptimization;
const wsCliCheckInterval = systemParams.webSocketClientCheckInterval;
const wsIsOnlineTimeout = systemParams.webSocketClientIsOnlineTimeout;
const quadMissionDataKey = systemParams.memcachedQuadMissionData;
const autoFlightSpeed = systemParams.quadAutoFlightSpeed;

class MatriceConnector {
    static initWebSocketServer(port, callback) {
        const webSocketServer = new WebSocket.Server({
            port: port,
            perMessageDeflate: parMessageDeflateOpts
        });
        MatriceConnector.setEvents(webSocketServer);
        MatriceConnector.webSocketServer = webSocketServer;
        callback();
    }

    static setEvents(webSocketServer) {
        webSocketServer.on('connection', function connection(webSocket, request) {
            MatriceConnector.isAlive = true;
            const interval = MatriceConnector.setPingPong(webSocket);
            const cliIP = MatriceConnector.getCliInfo(request);

            webSocket.on('message', function incoming(message) {
                let parsedData = MatriceConnector.parseMessage(message)
                MatriceConnector.saveData(parsedData);
            });

            webSocket.on('close', function close() {
                MatriceConnector.closeCli(interval);
                console.log(chalk.bgRed(chalk.white("Coordinator: quad app disconnected")));
            });

            console.log(chalk.underline(chalk.blue(`Coordinator: quad app connected, ${cliIP}`)));
        });
    }

    static closeCli(checkInterval, webSocket) {
        clearInterval(checkInterval);
    }

    static setPingPong(webSocket) {
        MatriceConnector.lastChecked = Date.now();
        const interval = setInterval(function ping() {
                webSocket.ping(MatriceConnector.noop);
                MatriceConnector.isAlive = false;
                if (Date.now() - MatriceConnector.lastChecked > wsIsOnlineTimeout ) {
                    console.log(chalk.bgRed(chalk.white("Coordinator: quad app takes too long to reply in ping-pong requests (might not be connected in my access point?)")));
                    webSocket.terminate();
                }
        }, wsCliCheckInterval);
        webSocket.on('pong', MatriceConnector.heartbeat);
        return interval;
    }

    static getCliInfo(request) {
        // if behind nginx, client ip is found from headers['x-forwarded-for']
        return (request.connection.remoteAddress)? request.connection.remoteAddress : request.headers['x-forwarded-for'].split(/\s*,\s*/)[0];
    }

    static parseMessage(message) {
        const parsedMessage = JSON.parse(message);
        let parsedData = {};
        parsedData["toc"] = Date.now();
        parsedData["lat"] = parsedMessage.latitude;
        parsedData["lon"] = parsedMessage.longitude;
        parsedData["relAlt"] = parsedMessage.relAltitude;
        parsedData["toAlt"] = parsedMessage.absAltitude;
        parsedData["alt"] = parseFloat(parsedData["toAlt"]) + parseFloat(parsedData["relAlt"]);
        parsedData["wind"] = parsedMessage.windWarning;
        parsedData["gps"] = parsedMessage.gpsSignalLevel;
        parsedData["sats"] = parsedMessage.satelliteCount;
        parsedData["batt"] = parsedMessage.remainingBattery;
        parsedData["rcBatt"] = parsedMessage.remainingRCBattery;
        parsedData["onMiss"] = parsedMessage.onMission;
        parsedData["gimR"] = parsedMessage.gimbalRoll;
        parsedData["gimP"] = parsedMessage.gimbalPitch;
        parsedData["gimY"] = parsedMessage.gimbalYaw;
        parsedData["pitch"] = parsedMessage.pitch;
        parsedData["roll"] = parsedMessage.roll;
        return parsedData;
    }

    static saveData(parsedData) {
        let serializedData = memcachedFunctions.serializeObject(parsedData);
        memcachedFunctions.setKey(quadLatestDataKey, serializedData, keyLifetime, function (err) {
            if (err) console.log(chalk.red(err));
        });
        memcachedFunctions.ensureListSize(quadMissionDataKey, function () {
            memcachedFunctions.prependKey(quadMissionDataKey, serializedData, function () {});
        });
    }

    // expects JSON object, with "route" key
    static sendToQuad(data, callback) {
        let message = {};
        message.route = data.route;
        message.flight_speed = autoFlightSpeed;
        console.log(message);
        //broadcast, adapt this for multiple quads
        MatriceConnector.webSocketServer.clients.forEach(function each(webSocket) {
            webSocket.send(JSON.stringify(message));
        });
        callback();
    }

    // ping-pong related
    static heartbeat() {
        MatriceConnector.isAlive = true;
        MatriceConnector.lastChecked = Date.now();
    }
    static noop() {}
}

module.exports.MatriceConnector = MatriceConnector;
