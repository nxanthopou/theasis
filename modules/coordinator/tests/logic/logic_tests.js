//http://localhost:3000/create_alarm?latitude=38.111975&longtitude=21.366578&risk_factor=0.91
const webSocketTestFunctions = require('../websockets/html_js.js')
const dbFunctions = require("../../src/database_functions.js");
const insertTestData = require("../db/insert_test_data.js");
const axios = require('axios');

const apiURL = 'http://localhost:3000'

function createAlarmRequest(latitude, longtitude, risk_factor) {
    axios.get(`${apiURL}/create_alarm?latitude=${latitude}&longtitude=${longtitude}&risk_factor=${risk_factor}`)
    .then(response => {
        console.log(response.data);
    })
    .catch(error => {
        console.log(error);
    });
}

async function getAlarm(id) {
    let alarm = await dbFunctions.getAlarm(id)
}

async function restartDB() {
    await insertTestData.initTestData();
    //let quad = await dbFunctions.getNearestQuad([38.111975, 21.366578]);
    //console.log(quad);
}

async function testLogic() {
    webSocketTestFunctions.openWebSockets();
}

async function runMe() {
 await restartDB();
 await testLogic();
 setTimeout(function() {
    createAlarmRequest(38.111975, 21.366578, 0.92);
 }, 2000);
}

runMe();
