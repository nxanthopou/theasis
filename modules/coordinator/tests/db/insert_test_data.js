// http://localhost:3000/create_quad?latitude=38.111975&longtitude=21.366578
// http://localhost:3000/get_quad?name=quad1
// http://localhost:3000/create_alarm?latitude=38.111975&longtitude=21.366578&risk_factor=0.91
// curl -X POST --data "latitude=38.111975&longtitude=21.366578" http://localhost:3000/create_alarm 
// curl -X GET http://localhost:3000/create_alarm?latitude=38.111975&longtitude=21.366578&risk_factor=0.9
const dbFunctions = require("../../src/database_functions.js");
const logicFunctions = require("../../src/logic_functions.js");

async function initTestData() {
    await dbFunctions.initDB(); //ensure custom index creation
    let quad = await dbFunctions.createQuad(38.111975, 21.366578);
}


module.exports.initTestData = initTestData
