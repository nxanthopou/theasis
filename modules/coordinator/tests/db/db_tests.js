const dbFunctions = require("../../src/database_functions.js");
const insertTestData = require("../db/insert_test_data.js");

async function testAlarmQuadRelations() {
    await insertTestData.initTestData(); 
    await dbFunctions.createQuad(38.111975, 21.366578);
    await dbFunctions.createAlarm(38.111975, 21.366578, 0.9);

    let alarm = await dbFunctions.getAlarmByLocation([38.111975, 21.366578]);
    let quad = await dbFunctions.getNearestQuad(alarm.location.coordinates);

    console.log(alarm._id);
    console.log(quad.name);
    await dbFunctions.relateAlarmQuad(quad.name, alarm._id);
    alarm = await dbFunctions.getAlarmByLocation([38.111975, 21.366578]);
    quad = await dbFunctions.getNearestQuad(alarm.location.coordinates);
    console.log(alarm);
    console.log(quad);
}

async function testAlarmOverlap() {
    await insertTestData.initTestData();
    await dbFunctions.createAlarm(38.111975, 21.366578, 0.9);
    await dbFunctions.createAlarm(38.112075, 21.366578, 0.8);
    await dbFunctions.createAlarm(38.112175, 21.366578, 0.7);
    await dbFunctions.createAlarm(38.121975, 21.366578, 0.6);
    await dbFunctions.createAlarm(38.122975, 21.366578, 0.5);
    let alarms = await dbFunctions.getNearestAlarms([38.111975, 21.366578], 2500);
    let quad = await dbFunctions.getNearestQuad([38.111975, 21.366578]);
    console.log(alarms, quad);
}

testAlarmOverlap()
