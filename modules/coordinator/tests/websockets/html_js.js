const WebSocket = require('ws');
const wssURL = 'ws://localhost:3001'
var controlCliWS;
var planCliWS;
var controlMessage = {};
var planMessage = {};

function initPlanMessage() {
    planMessage.start_location = [];
    planMessage.end_location = [];
    planMessage.route = [
            [38.111975, 21.466578],
            [38.111975, 21.456578],
            [38.111975, 21.446578],
            [38.111975, 21.436578],
            [38.111975, 21.426578],
            [38.111975, 21.416578],
            [38.111975, 21.411578],
            [38.111975, 21.405578],
            [38.111975, 21.396578],
            [38.111975, 21.386578],
            [38.111975, 21.382578],
            [38.111975, 21.380578],
            [38.111975, 21.375578],
            [38.111975, 21.370578],
            [38.111975, 21.366578]
            ];
    }

function closePlanWebSocket() {
    planCliWS.close();
}

function sendPlanMessage() {
    planCliWS.send(JSON.stringify(planMessage));
}

function initPlanWebsocket(webSocketURL) {
    initPlanMessage();
    planCliWS = new WebSocket(wssURL, "navPlanning");
        planCliWS.onopen = function() {
            //sendMessage();
            console.log("Planning WebSocket opened!");
        };

        planCliWS.onmessage = function (evt) { 
            const received_msg = JSON.parse(evt.data);
            console.log(received_msg);
            planMessage.startLoc = received_msg.startLoc;
            planMessage.endLoc = received_msg.endLoc;
            planMessage.route[0] = received_msg.startLoc;
            planMessage.route[-1] = received_msg.endLoc;
            //computePath(startLocation, endLocation);
            sendPlanMessage();
        };

        planCliWS.onclose = function() {
            console.log("Planning WebSocket is closed..."); 
        };
}

function initControlMessage() {
    controlMessage.current_location = [38.111975, 21.366578];
    controlMessage.flight_time = 0;
    controlMessage.battery_state = 95;
    controlMessage.battery_temp = 25;
    controlMessage.route = [];
    controlMessage.state = "READY";
    controlMessage.nextWaypoint = [];
    controlMessage.gps_signal_level = 25;
    controlMessage.battery_warning = false;
    controlMessage.attitude = [];
    controlMessage.is_failsafe = false;
    controlMessage.heading = 0.0;
    controlMessage.targetLocation = [];
}

function initControlWebsocket() {
    initControlMessage();
    controlCliWS = new WebSocket(wssURL, "navControl");
        controlCliWS.onopen = function() {
            //sendMessage();
            console.log("Control WebSocket opened!");
        };

        controlCliWS.onmessage = function (evt) { 
            const received_msg = JSON.parse(evt.data);
            if  (controlMessage.route != received_msg.route) {
                console.log("new route requested");
                //newMission();
                controlMessage.route = received_msg.route;
            };
            controlMessage.route = received_msg.route;
            console.log(controlMessage.route);
            sendControlMessage(controlMessage);
        };

        controlCliWS.onclose = function() {
            console.log("Control WebSocket is closed..."); 
        };
}

function closeControlWebSocket() {
    controlCliWS.close();
}

function sendControlMessage() {
    controlCliWS.send(JSON.stringify(controlMessage));
}

function closeWebSockets() {
    closePlanWebSocket();
    closeControlWebSocket();
}

function openWebSockets() {
    initPlanWebsocket();
    initControlWebsocket();
}

module.exports.openWebSockets = openWebSockets
module.exports.closeWebSockets = closeWebSockets

openWebSockets();
