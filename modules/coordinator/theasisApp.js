/*
Xanthopoulos Nikitas, 2019
nxanthopou@ceid.upatras.gr
*/

'use strict';
const chalk = require('chalk');
const pathPlanningConnection = require("./src/pathPlanningConnection.js").PathPlanningConnector;
const matriceConnection = require("./src/matrice210Connection.js").MatriceConnector;
const uiConnection = require("./src/uiConnection.js").UserInterfaceConnector;
const memcachedFunctions = require("./src/memcachedFunctions.js").MemcachedClient;
const logicFunctions = require("./src/logicFunctions.js").LogicFunctions;
const httpServer = require("./src/httpServer.js").HttpServer;
const systemParams = require("./src/systemParameters.js");

const httpPort = systemParams.httpPort;
const pathPlanningListeningPort = systemParams.wsPathPlanningPort;
const matriceListeningPort = systemParams.wsMatricePort;
const uiListeningPort = systemParams.wsUIPort;
const memcachedPort = systemParams.memcachedPort;
const memcachedHost = systemParams.memcachedHost;

function initSystemServer() {
    // http server initialization
    httpServer.initHTTPServer(httpPort, function () {
        console.log(chalk.yellow(`Coordinator: HTTP server running on port ${httpPort}`));
    });
    // memory initilization
    memcachedFunctions.initMemcachedCli(memcachedHost, memcachedPort, function () {
        memcachedFunctions.setDBKeys();
        console.log(chalk.magenta(`Coordinator: Connected to memory, host: ${memcachedHost}, port: ${memcachedPort}`));
    });
    // path planning middleware (websocket) initialization
    pathPlanningConnection.initWebSocketServer(pathPlanningListeningPort, function () {
        console.log(chalk.cyan(`Coordinator: Waiting path planning on port: ${pathPlanningListeningPort}`));
    });
    // uav middleware (websocket) initialization
    matriceConnection.initWebSocketServer(matriceListeningPort, function () {
        console.log(chalk.blue(`Coordinator: Waiting Matrice app on port: ${matriceListeningPort}`));
    });
    // user interface, alarm notifications
    uiConnection.initWebSocketServer(uiListeningPort, function () {
        console.log(chalk.yellow(`Coordinator: Waiting UI apps on port: ${uiListeningPort}`));
    });

    // proceed with stored alarm if it exists (in case of app restart)
    logicFunctions.processQueuedAlarm();
}

initSystemServer();

// mixanologoi parking gwnia pros fylakio: curl -XGET "http://localhost:3000/new_alarm?latitude=38.289410&longitude=21.785558"
// gipedo xortari panw-deksia gwnia konta: curl -XGET "http://localhost:3000/new_alarm?latitude=38.282671&longitude=21.788995"
// gipedo xortarideksia terma: curl -XGET "http://localhost:3000/new_alarm?latitude=38.282523&longitude=21.789176"

