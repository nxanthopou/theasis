## Overview

Theasis' is using `DJI Matrice 210 Series 1` as a mobile sensor device for alarm validation. The application is build with **Android SDK v28, build tools v28.0.3**. 

The purpose of this application is the implementation of the following functionalities:  
- Autonomous missions. It expects an array of waypoints `[latitude, longitude, altitude]` through the WebSocket client. Once the array has been received, a `WaypointMission` is built as per DJI's SDK - ensure **[DJI's Waypoint Mission stadards](https://developer.dji.com/api-reference/android-api/Components/SDKError/DJIError_DJIMissionManagerError.html)**.  
- Data transmission to Theasis. Video feeds and quadcopter-related information are exposed to Theasis.  
- A fail-safe simple user interface. Video feeds are projected on the connected Android device and missions are paused/stoped/resumed using touchscreen's buttons.

* Video transmission is accomplished through `RTMP` protocol.  
* Quadcopter-related information is transmitted through `WebSocket` protocol.  

- - -  

## Build / Deployment

This application was built and deployed using `Android Studio`, `Android Device Bridge`. Integrated libraries are mentioned at the bottom of this doc.  

- **To achieve full functionality, XT and X5S cameras must be mounted.**
- **Application needs USB permissions once remote controller is connected/powered on.**

- - - 

## Software structure

- `AndroidManifest.xml`: Create your application key for SDK activation (using DJI's site/service) and replace it:

```xml
    <meta-data
        android:name="com.dji.sdk.API_KEY"
        android:value="your DJI SDK API key" />
```


- `ConnectionActivity.java`: Appication's first page (UI):
    - Registers application's SDK.
    - Initializes User Interface.
    - Initializes `WebSocket` connection with Theasis.
    - Once remote controller and quadcopter have been powered this page will automatically switch to application's second UI page.
    
- `FlightActivity.java`: Application's second page (UI):
    - Provides textures for optical and thermal video feeds.
    - Provides the "safety" buttons, e.g. stop/pause/resume mission.
    - Displays some quadcopter-related information, e.g latitude, longitude, altitude, diagnostics.
    
- `MApplication.java`: Application's entry point. 
- `Matrice210Application.java`: 
    - Calls setters for required features.
    - Initiates timers.
    - Provides methods for logging purposes.
    - Provides a general error handling method.
    
- `Matrice210DataExposal.java`: Provides methods responsible for exposing data to Theasis. These methods set keys and values of the transmitted JSON object and start/stop/switch livestream.

- `Matrice210Getters.java`: DJI Android SDK getters' wrappers.
    
- `Matrice210Setters.java`: DJI Android SDK setters' wrappers. Features that are directly related with DJI Android SDK are configured with this file's methods.
    - Cameras: Bandwidths/focus mode, thermal palette/isotherm/scene/gain mode.
    - Gimbals: Gimbals are set to move synchronized.
    - Flight limitations (altitude - radius).
    - Gimbal spin: The quadcopter will rotate around yaw if requested angle is beyond gimbals current capability.
    - Connection failsafe: The quadcopter will return to home location if connection with remote controller is lost.
    - Safe go-home and go-home altitude: Collision sensors are not enabled by default when returning to home.
    - Flight controller data: A callback that runs with a frequency of 10Hz and updates quadcopter's data (yaw/pitch/roll/latitude/longitude/etc).
    - Flight assistant: Collision (upwards as well) avoidance, return to home collision avoidance, smart return to home.
    - Diagnostics.
    
- `Matrice210Mission.java`: Autonomous missions related software. 
    - `initiatePPMission` expects an array and an int as arguments. The array consists of X cells with 3 values each [latitude, longitude, altitude] and the int is the auto-flight-speed. If no errors are found after building and loading the mission, the quadcopter will take off.
    - `buildWaypointMission` returns a configured `WaypointMission` to be loaded onto quadcopter. Two triggers are added to the last waypoint/target position, one which rotates cameras downwards and one that orders the quadcopter to hover for X milliseconds. 
    
    
- `TheasisWebSocket.java`: WebSocket client. `on-message` event triggers the autonomous mission.  

- `Matrice210Parameters.java`: Provides single-file parameterization for this application.  

- Logs are stored on Android device, `/sdcard/theasis.{err|log}`

### Parameterization

DJI CrystalSky tablet has no preinstalled text editor - parameters of the application cannot be changed through a text file existing in the tablet. To change them, one has to re-compile and re-upload the application.  

- Server IP:  
```java
        protected static final String rtmpServerUrlDual = "rtmp://192.168.12.1:1935/live/quad";
        protected static final String apiWSURL = "ws://192.168.12.1:3002/";
```

- Autonomous missions, [DJI docs](https://developer.dji.com/api-reference/android-api/Components/Missions/DJIWaypointMission.html):
```java
        protected static int componentsDesired = 9;
        protected static int maxFlightHeight = 250;
        protected static float autoFlightSpeed = 5.0f;
        protected static int goHomeHeight = 50;
        protected static int feedSwitchPeriod = 15000;
        protected static int apiConnectionPeriod = 1000;
        protected static float maxFlightSpeed = 15.0f;
        protected static float batteryThreshold = 20f;
        protected static boolean quitMissionOnRCLose = true;  
        protected static final WaypointMissionFinishedAction finishAction = WaypointMissionFinishedAction.GO_HOME; 
        protected static final WaypointMissionFlightPathMode flightPathMode = WaypointMissionFlightPathMode.NORMAL;
        protected static final WaypointMissionGotoWaypointMode waypointGoToMode = WaypointMissionGotoWaypointMode.POINT_TO_POINT;
        protected static final WaypointMissionHeadingMode waypointHeadingMode = WaypointMissionHeadingMode.AUTO;
```   

- Thermal camera, [DJI docs](https://developer.dji.com/api-reference/android-api/Components/Camera/DJICamera.html#thermal):  
```java
        protected static SettingsDefinitions.ThermalPalette thermalCameraColorPalette = SettingsDefinitions.ThermalPalette.BLACK_HOT;
        protected static SettingsDefinitions.ThermalScene thermalScene = SettingsDefinitions.ThermalScene.DEFAULT;
        protected static SettingsDefinitions.ThermalGainMode  thermalGainMode = SettingsDefinitions.ThermalGainMode.LOW;
        protected static SettingsDefinitions.FocusMode cameraFocusMode = SettingsDefinitions.FocusMode.AUTO;
        protected static boolean thermalIsothermEnabled = true;
        protected static int isothermT1 = 90;
        protected static int isothermT2 = 95;
        protected static int isothermT3 = 100;
        protected static SettingsDefinitions.ThermalIsothermUnit thermalISOUnit = SettingsDefinitions.ThermalIsothermUnit.PERCENTAGE;
        protected static float transcodingRate = 1.f;
        protected static int thermalDDE = 0;
        protected static int thermalACE = 0;
        protected static int thermalSSO = 0;
        protected static int thermalContrast = 0;
        protected static int thermalBrightness = 5000;
```
        
**componentsDesired** is the counter of the connected parts. Batteries (x2), Cameras (x2), Gimbal (x2), Airlink, Remote Controller, Flight Controller.
The check is completed on application start up. If detected parts' counter is less than this value, a part is probably malfunctioning and needs to be remounted.
The counter is displayed in application's first user interface page. Remember to give Theasis USB permissions.  

`finishAction`, `flightPathMode`, `waypointGoToMode`, `waypointHeadingMode` options can be found in "Related" sector of [DJI's documentation](https://developer.dji.com/api-reference/android-api/Components/Missions/DJIWaypointMission.html). 
`thermalCameraColorPalette` sets the thermal palette (colouring) of the thermal feed, see [DJI's documentation](https://developer.dji.com/api-reference/android-api/Components/Camera/DJICamera_DJICameraSettingsDef.html#djicamera_djicamerathermalpalette_inline) for other options.  
`isothermT1`, `isothermT2`, `isothermT3` are the lower, mid, upper thresholds respectively.  
`thermalISOUnit` can be changed to `CELSIUS`, but change isotherm thresholds accordingly.  
`thermalDDE`, `thermalACE`, `thermalSSO` are digital filters provided by DJI. `thermalScene` must be set to `MANUAL` to change them.  


- - -  

## Additional libraries
Packages were installed via **gradle**:  

* `com.dji:dji-sdk:4.10`: DJI API.
* `tech.gusavila92:java-android-websocket-client:1.2.1`: WebSocket client.  

- - -  

## Demo

`ConnectionActivity.java`:

![img](first_page.jpg)


`FlightActivity.java`:

![img](second_page.jpg)


- - -  

## Notes

- [DJI Android SDK](https://developer.dji.com/api-reference/android-api/Components/SDKManager/DJISDKManager.html)
- SDK has bugs some of which are currently being fixed. For example, `secoundary` instead of `secondary` when selecting the thermal feed.
- Video feeds are green on app startup. Restart the application to have a clear view.
- Thermal and optical feeds cannot be simultaneously transmitted. Hence, a timer that switches the feed between those two is implemented.
- Ensure that XT is connected correctly. If it doesn't rotate on quadcopter startup like X5S, remount it.
- Collision avoidance: after laboratory testing it appears that the quadcopter will not try to move around a detected object, as per DJI's official specifications and SDK's documentation for Matrice 210. It detects the collision, stops its movement, and retries forward movement after a short interval.  
