/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */

package xanthopoulos.nikitas.theasis;

import dji.common.camera.SettingsDefinitions;
import dji.common.mission.waypoint.WaypointMissionFinishedAction;
import dji.common.mission.waypoint.WaypointMissionFlightPathMode;
import dji.common.mission.waypoint.WaypointMissionGotoWaypointMode;
import dji.common.mission.waypoint.WaypointMissionHeadingMode;

public class Matrice210Parameters {
    // next two IPS need to match coordinator's (.env WIFI_NETWORK_ENTRY if run with docker)
    protected static String rtmpServerUrlDual = "rtmp://192.168.12.1:1935/live/quad";
    protected static String apiWSURL = "ws://192.168.12.1:3002/";

    // BATTERY/GIMBAL/CAMERA x2, AIRLINK/FLIGHTCONTROLLER/RC x1
    protected static int componentsDesired = 9;
    protected static int maxFlightHeight = 250;
    protected static float autoFlightSpeed = 5.0f;
    protected static int goHomeHeight = 50;
    protected static int feedSwitchPeriod = 15000; //msecs
    protected static int apiConnectionPeriod = 1000; //msecs
    protected static float maxFlightSpeed = 15.0f;
    protected static float batteryThreshold = 20f;
    protected static boolean quitMissionOnRCLose = true;

    protected static WaypointMissionFinishedAction finishAction = WaypointMissionFinishedAction.GO_HOME; //if distance from home<20m, land immediately
    protected static WaypointMissionFlightPathMode flightPathMode = WaypointMissionFlightPathMode.NORMAL;
    protected static WaypointMissionGotoWaypointMode waypointGoToMode = WaypointMissionGotoWaypointMode.SAFELY;
    protected static WaypointMissionHeadingMode waypointHeadingMode = WaypointMissionHeadingMode.AUTO;
    protected static SettingsDefinitions.ThermalPalette thermalCameraColorPalette = SettingsDefinitions.ThermalPalette.BLACK_HOT;
    protected static SettingsDefinitions.ThermalScene thermalScene = SettingsDefinitions.ThermalScene.MANUAL;
    protected static SettingsDefinitions.ThermalGainMode  thermalGainMode = SettingsDefinitions.ThermalGainMode.LOW;
    protected static SettingsDefinitions.FocusMode cameraFocusMode = SettingsDefinitions.FocusMode.AUTO;
    protected static boolean thermalIsothermEnabled = true;
    protected static int isothermT1 = 90;
    protected static int isothermT2 = 95;
    protected static int isothermT3 = 100;
    protected static SettingsDefinitions.ThermalIsothermUnit thermalISOUnit = SettingsDefinitions.ThermalIsothermUnit.PERCENTAGE;
    protected static float transcodingRate = 1.f;
    protected static int thermalDDE = 0;
    protected static int thermalACE = 0;
    protected static int thermalSSO = 0;
    protected static int thermalContrast = 0;
    protected static int thermalBrightness = 5000;
    protected static boolean autoGimbalSpin = false;


    protected static int webSocketConnectionTimeout = 5000;
    protected static int webSocketReadTimeout = 20000;
    protected static int webSocketAutoReconnectionTimeout = 2500;

    public static final String FLAG_CONNECTION_CHANGE = "connection_change"; // Activity/DJI SDK related
}
