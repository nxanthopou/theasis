/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */

package xanthopoulos.nikitas.theasis;

import android.util.Log;

import java.util.List;

import dji.common.error.DJIError;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.mission.waypoint.WaypointMission;
import dji.common.mission.waypoint.WaypointMissionState;
import dji.common.model.LocationCoordinate2D;
import dji.common.util.CommonCallbacks;
import dji.sdk.airlink.AirLink;
import dji.sdk.base.BaseProduct;
import dji.sdk.base.DJIDiagnostics;
import dji.sdk.battery.Battery;
import dji.sdk.camera.Camera;
import dji.sdk.flightcontroller.Compass;
import dji.sdk.flightcontroller.FlightAssistant;
import dji.sdk.flightcontroller.FlightController;
import dji.sdk.gimbal.Gimbal;
import dji.sdk.mission.MissionControl;
import dji.sdk.mission.waypoint.WaypointMissionOperator;
import dji.sdk.products.Aircraft;
import dji.sdk.remotecontroller.RemoteController;
import dji.sdk.sdkmanager.DJISDKManager;
import dji.sdk.sdkmanager.LiveStreamManager;

public class Matrice210Getters {
    protected static DJISDKManager getSDKManager() {
        return DJISDKManager.getInstance();
    }

    //returns LocationCoordinate2D on static var through callback
    protected static void getHomeLocation() {
        if (Matrice210Getters.getFlightController() != null) {
            Matrice210Getters.getFlightController().getHomeLocation(new CommonCallbacks.CompletionCallbackWith<LocationCoordinate2D>() {
                @Override
                public void onSuccess(LocationCoordinate2D locationCoordinate2D) {
                    //might return latitude/longitude "NaN"
                    Matrice210Application.homeLocation = locationCoordinate2D;
                }

                @Override
                public void onFailure(DJIError djiError) {
                    Log.v("homeLocERR", djiError.getDescription());
                }
            });
        }
    }

    // needed by FlightActivity (to show videos on tablet surface) activity
    protected static synchronized BaseProduct getProductInstance() {
        return getSDKManager().getProduct();
    }

    protected static FlightController getFlightController() {
        return ((Aircraft) getProductInstance()).getFlightController();
    }

    protected static FlightControllerState getFlightControllerState() {
        return getFlightController().getState();
    }

    protected static FlightAssistant getAssistant() {
        return getFlightController().getFlightAssistant();
    }

    protected static MissionControl getMissionControl() {
        return getSDKManager().getMissionControl();
    }

    protected static WaypointMissionOperator getWaypointMissionOperator() {
        return getMissionControl().getWaypointMissionOperator();
    }

    protected static WaypointMission getMission() {
        return getWaypointMissionOperator().getLoadedMission();
    }

    protected static WaypointMissionState getMissionState() {
        return getWaypointMissionOperator().getCurrentState();
    }

    protected static LiveStreamManager getLSManager() {
        return getSDKManager().getLiveStreamManager();
    }

    protected static Gimbal getGimbal() {
        return getProductInstance().getGimbal();
    }

    protected static List<Camera> getCameras() {
        return getProductInstance().getCameras();
    }

    protected static List<DJIDiagnostics> getDiagnostics() {
        return Matrice210Application.diagnostics;
    }

    protected static RemoteController getRC() {
        return ((Aircraft) getProductInstance()).getRemoteController();
    }

    protected static Battery getBattery() {
        return getProductInstance().getBattery();
    }

    protected static AirLink getAirlink() {
        return getProductInstance().getAirLink();
    }

    protected static Compass getCompass() {
        return getFlightController().getCompass();
    }
}
