/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */
package xanthopoulos.nikitas.theasis;

import android.app.Application;
import android.content.Context;

import com.secneo.sdk.Helper;

public class MApplication extends Application {
    private Matrice210Application theasisDemoApp;

    @Override
    protected void attachBaseContext(Context paramContext) {
        super.attachBaseContext(paramContext);
        Helper.install(MApplication.this);
        if (theasisDemoApp == null) {
            theasisDemoApp = new Matrice210Application();
            theasisDemoApp.setContext(MApplication.this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        theasisDemoApp.onCreate();
    }

}
