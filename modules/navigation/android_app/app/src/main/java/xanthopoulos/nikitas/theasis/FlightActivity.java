/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */
package xanthopoulos.nikitas.theasis;

import android.app.Activity;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import dji.midware.usb.P3.UsbAccessoryService;
import dji.sdk.base.DJIDiagnostics;
import dji.sdk.codec.DJICodecManager;


public class FlightActivity extends Activity implements TextureView.SurfaceTextureListener, View.OnClickListener {
    private static final String TAG = FlightActivity.class.getName();
    protected static Activity instance = null;
    private TextureView mVideoSurface1 = null;
    private TextureView mVideoSurface2 = null;
    private TextView mInfo1Text, mInfo2Text, mDiagnosticsTextView, mVisErrosTextView = null;
    private DJICodecManager mCodecManager1 = null;
    private DJICodecManager mCodecManager2 = null;
    private Button mBtnMP, mBtnPM, mBtnSM, mBtnRM, mBtnLSStart, mBtnLSStop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_flight);
        initUI();
        instance = this;
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }

    private void buttonInit(Button btn, int btnId) {
        btn = (Button) findViewById(btnId);
        btn.setOnClickListener(this);
        btn.setEnabled(true);
    }

    private void initUI() {
        mVideoSurface1 = (TextureView)findViewById(R.id.left_camera_surface);
        mVideoSurface2 = (TextureView)findViewById(R.id.right_camera_surface);
        mVideoSurface1.setSurfaceTextureListener(this);
        mVideoSurface2.setSurfaceTextureListener(this);
        mInfo1Text = (TextView) findViewById(R.id.info1);
        mInfo2Text = (TextView) findViewById(R.id.info2);
        mDiagnosticsTextView = (TextView) findViewById(R.id.diagnostics_textview);
        mVisErrosTextView = (TextView) findViewById(R.id.visibleErr_textview);
        buttonInit(mBtnLSStart, R.id.main_pg);
        buttonInit(mBtnLSStop, R.id.start_ls);
        buttonInit(mBtnRM, R.id.stop_ls);
        buttonInit(mBtnPM, R.id.pause_miss);
        buttonInit(mBtnSM, R.id.stop_miss);
        buttonInit(mBtnRM, R.id.resume_miss);
        textUpdateTimer();
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        if (Matrice210Application.codecManagerRGB == null && surface == mVideoSurface1.getSurfaceTexture())
            Matrice210Application.codecManagerRGB =  new DJICodecManager(this, surface, width, height,  UsbAccessoryService.VideoStreamSource.Camera);
        if (Matrice210Application.codecManagerThermal == null && surface == mVideoSurface2.getSurfaceTexture())
            Matrice210Application.codecManagerThermal =  new DJICodecManager(this, surface, width, height,  UsbAccessoryService.VideoStreamSource.Fpv);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        Log.e(TAG, "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {

        if (Matrice210Application.codecManagerRGB != null  && surface == mVideoSurface1.getSurfaceTexture() ) {
            Matrice210Application.codecManagerRGB.cleanSurface();
            Matrice210Application.codecManagerRGB = null;
        }
        if (Matrice210Application.codecManagerThermal != null  && surface == mVideoSurface2.getSurfaceTexture() ) {
            Matrice210Application.codecManagerThermal.cleanSurface();
            Matrice210Application.codecManagerThermal = null;
        }

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_pg: {
                Intent intent = new Intent(this, ConnectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivityIfNeeded(intent, 0);
                break;
            }
            case R.id.pause_miss: {
                Matrice210Mission.pauseMission();
                break;
            }
            case R.id.stop_miss: {
                Matrice210Mission.stopMission();
                break;
            }
            case R.id.resume_miss: {
                Matrice210Mission.resumeMission();
                break;
            }
            case R.id.start_ls: {
                Matrice210DataExposal.startStream();
                break;
            }
            case R.id.stop_ls: {
                Matrice210DataExposal.stopStream();
                break;
            }
            default:
                break;
        }
    }
    private void textUpdateTimer() {
        Timer timer = new Timer();
        final DecimalFormat decimalFormat5 = new DecimalFormat("#.00000");
        final DecimalFormat decimalFormat1 = new DecimalFormat("#.0");
        final DecimalFormat decimalFormat0 = new DecimalFormat("#.");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        List<DJIDiagnostics> diag = Matrice210Getters.getDiagnostics();
                        String diagnostics = "";
                        if (diag != null) {
                            diagnostics = "Diagnostics: ";
                            for (DJIDiagnostics dd : diag) {
                                diagnostics = diagnostics + dd.getReason() + ", " + dd.getCode() + " " + dd.getSolution();
                            }
                        }
                        final String connectedToApi = (!TheasisWebSocket.isConnected)? "No" : "Yes";
                        final String info1 = Matrice210Application.gpsSignalLevel.toString()  +
                                ", isRGBStream: " + String.valueOf(Matrice210Application.isOpticalStream) +
                                ", Theasis: " + connectedToApi;

                        final String info2 = decimalFormat5.format(Matrice210Application.latitude) +
                                        ", " + decimalFormat5.format(Matrice210Application.longitude) +
                                        ", yaw: " + decimalFormat1.format(Matrice210Application.yaw) +
                                        "\nabsAlt: " + decimalFormat0.format(Matrice210Application.absAltitude) +
                                        ", relAlt: " + decimalFormat0.format(Matrice210Application.relAltitude);

                        mInfo1Text.setText(info1);
                        mInfo2Text.setText(info2);
                        String visErr = (Matrice210Application.visibleErrorsStr == null)?
                                "Mission errors: All ok." :
                                Matrice210Application.visibleErrorsStr;
                        mVisErrosTextView.setText(visErr);
                        mDiagnosticsTextView.setText(diagnostics);

                    }
                });
            }

        }, 0, 1000); // change video stream source every feedSwitchPeriod seconds
    }
}
