/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */

package xanthopoulos.nikitas.theasis;

import android.util.Log;
import java.util.Date;
import org.json.JSONObject;
import dji.sdk.sdkmanager.LiveStreamManager;

public class Matrice210DataExposal {

    protected static void setupAPIMessage() {
        Matrice210Application.apiTXMessage = new JSONObject();
        Matrice210DataExposal.setKey("timestamp", new Date().toString());
        Matrice210DataExposal.setKey("gpsSignalLevel", "-1");
        Matrice210DataExposal.setKey("satelliteCount", "-1");
        Matrice210DataExposal.setKey("windWarning", "-1");
        Matrice210DataExposal.setKey("latitude", "-1");
        Matrice210DataExposal.setKey("longitude", "-1");
        Matrice210DataExposal.setKey("relAltitude", "-1");
        Matrice210DataExposal.setKey("absAltitude", "-1");
        Matrice210DataExposal.setKey("roll", "-1");
        Matrice210DataExposal.setKey("pitch", "-1");;
        Matrice210DataExposal.setKey("yaw", "-1");
        Matrice210DataExposal.setKey("remainingBattery", "-1");
        Matrice210DataExposal.setKey("remainingRCBattery", "-1");
        Matrice210DataExposal.setKey("gimbalRoll", "-1");
        Matrice210DataExposal.setKey("gimbalPitch", "-1");
        Matrice210DataExposal.setKey("gimbalYaw", "-1");
        Matrice210DataExposal.setKey("onMission", "false");
        Matrice210DataExposal.setKey("isQuadConnected", "false");
    }

    protected static void setMessageValues() {
        Matrice210DataExposal.setKey("isQuadConnected", "true");
        Matrice210DataExposal.setKey("timestamp", new Date().toString());
        Matrice210DataExposal.setKey("gpsSignalLevel", Matrice210Application.gpsSignalLevel.toString());
        Matrice210DataExposal.setKey("satelliteCount", String.valueOf(Matrice210Application.satelliteCount));
        Matrice210DataExposal.setKey("windWarning", Matrice210Application.windWarning.toString());
        Matrice210DataExposal.setKey("latitude",  String.valueOf(Matrice210Application.latitude));
        Matrice210DataExposal.setKey("longitude", String.valueOf(Matrice210Application.longitude));
        Matrice210DataExposal.setKey("relAltitude", String.valueOf(Matrice210Application.relAltitude));
        Matrice210DataExposal.setKey("absAltitude", String.valueOf(Matrice210Application.absAltitude));
        Matrice210DataExposal.setKey("roll", String.valueOf(Matrice210Application.roll));
        Matrice210DataExposal.setKey("pitch", String.valueOf(Matrice210Application.pitch));
        Matrice210DataExposal.setKey("yaw", String.valueOf(Matrice210Application.yaw));
        Matrice210DataExposal.setKey("remainingBattery", String.valueOf(Matrice210Application.batteryLifeLeft));
        Matrice210DataExposal.setKey("remainingRCBattery", String.valueOf(Matrice210Application.rcBatteryLifeLeft));
        Matrice210DataExposal.setKey("gimbalRoll", String.valueOf(Matrice210Application.gimbalRoll));
        Matrice210DataExposal.setKey("gimbalPitch", String.valueOf(Matrice210Application.gimbalPitch));
        Matrice210DataExposal.setKey("gimbalYaw", String.valueOf(Matrice210Application.gimbalYaw));
    }

    protected static void setKey(String label, String value) {
        try {
            Matrice210Application.apiTXMessage.put(label, value);
        } catch (Exception e) {
            Log.v("TE: setDataTXMessage", e.getMessage());
        }
    }

    protected static void setupConnectionToAPI() {
        TheasisWebSocket.createWebSocketClient();
    }

    protected static void startStream() {
        if (!Matrice210Getters.getLSManager().isStreaming()) {
            new Thread() {
                @Override
                public void run() {
                    Matrice210Getters.getLSManager().setVideoSource(LiveStreamManager.LiveStreamVideoSource.Secoundary);
                    Matrice210Getters.getLSManager().setLiveUrl(Matrice210Parameters.rtmpServerUrlDual);
                    Matrice210Getters.getLSManager().setVideoEncodingEnabled(false);
                    Matrice210Getters.getLSManager().setAudioMuted(true);
                    int result = Matrice210Getters.getLSManager().startStream();
                    Matrice210Getters.getLSManager().setStartTime();
                    //Matrice210Application.switchStreamTimerInit();

                }
            }.start();
        }
    }

    protected static void stopStream() {
        Matrice210Getters.getLSManager().stopStream();
    }

    protected static void secureLiveStream() {
        if (Matrice210Getters.getLSManager().isStreaming()) {
            try {
                Thread.sleep(500);
                secureLiveStream();
            } catch (Exception e) {
                Log.v("TE: ls sleep er", e.getMessage());
            }
        } else {
            String videoFeed = ( Matrice210Application.isOpticalStream)? "false" : "true";
            Matrice210DataExposal.setKey("videoFeed", videoFeed);
        }
    }

    protected static void changeLiveStreamSource() {
        Matrice210Getters.getLSManager().stopStream();
        LiveStreamManager.LiveStreamVideoSource source = ( Matrice210Application.isOpticalStream)? LiveStreamManager.LiveStreamVideoSource.Primary : LiveStreamManager.LiveStreamVideoSource.Secoundary;
        Matrice210Getters.getLSManager().setVideoSource(source);
        Matrice210Application.isOpticalStream = ! Matrice210Application.isOpticalStream;
        secureLiveStream();
        new Thread() {
            @Override
            public void run() {
                int result =  Matrice210Getters.getLSManager().startStream();
                //getLSManager().setStartTime();
            }
        }.start();
    }
}
