/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */
package xanthopoulos.nikitas.theasis;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import dji.common.error.DJIError;
import dji.common.error.DJISDKError;
import dji.common.util.CommonCallbacks;
import dji.internal.util.CompletionTester;
import dji.sdk.base.BaseComponent;
import dji.sdk.base.BaseProduct;
import dji.sdk.products.Aircraft;
import dji.sdk.sdkmanager.DJISDKInitEvent;
import dji.sdk.sdkmanager.DJISDKManager;

public class ConnectionActivity extends Activity implements  View.OnClickListener {
    private static int componentsConnected = 0;
    private static final String TAG = ConnectionActivity.class.getName();
    private static final int REQUEST_PERMISSION_CODE = 12345;
    private TextView mTextConnectionStatus, mTextInitProcess;
    private TextView mTextSDKInfo;
    private TextView mVersionTv;
    private TextView mCompoConnetedStatus;
    private Button mBtnOpen, mBtnClose;
    private Handler mHandler;
    private List<String> missingPermission = new ArrayList<>();
    private AtomicBoolean isRegistrationInProgress = new AtomicBoolean(false);
    private static final String[] REQUIRED_PERMISSION_LIST = new String[]{
            Manifest.permission.VIBRATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
    };
    protected BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            refreshSDKRelativeUI();
        }
    };

    protected void writeParamsFile(File file) {
        try {
            JSONObject params = new JSONObject();

            params.put("rtmpServerUrlDual", Matrice210Parameters.rtmpServerUrlDual);
            params.put("apiWSURL", Matrice210Parameters.apiWSURL);
            params.put("maxFlightHeight", Matrice210Parameters.maxFlightHeight);
            params.put("autoFlightSpeed", Matrice210Parameters.autoFlightSpeed);
            params.put("goHomeHeight", Matrice210Parameters.goHomeHeight);
            params.put("batteryThreshold", Matrice210Parameters.batteryThreshold);
            params.put("thermalDDE", Matrice210Parameters.thermalDDE);
            params.put("thermalACE", Matrice210Parameters.thermalACE);
            params.put("thermalSSO", Matrice210Parameters.thermalSSO);
            params.put("isothermT1", Matrice210Parameters.isothermT1);
            params.put("isothermT2", Matrice210Parameters.isothermT2);
            params.put("isothermT3", Matrice210Parameters.isothermT3);

            String paramsStr = params.toString();
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(paramsStr);
            writer.close();
        } catch (IOException e) {
            Matrice210Application.writeToErrFile("could not write json params file" + e.toString());
            Log.v("json params file: ", "could not write" + e.toString());
        } catch (JSONException e) {
            Matrice210Application.writeToErrFile("could not parse json params file (write)" + e.toString());
            Log.v("json params file: ", "could not parse (write)" + e.toString());
        }
    }

    protected JSONObject readParamsFile() {
        File file = new File("/sdcard/theasis_parameters.json");
        if (!file.exists()) {
            writeParamsFile(file);
        }
        StringBuilder text = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            String paramsStr = text.toString();
            JSONObject json = new JSONObject(paramsStr);
            return json;
        }
        catch (IOException e) {
            Matrice210Application.writeToErrFile("could not read json params file" + e.toString());
            Log.v("json params file: ", "could not read" + e.toString());
        }
        catch (JSONException e) {
            Matrice210Application.writeToErrFile("could not parse json params file"+ e.toString());
            Log.v("json params file: ", "could not parse" + e.toString());
        }
        return new JSONObject();
    }

    protected void overrideParameters(JSONObject json){
     try {
            Matrice210Parameters.rtmpServerUrlDual = json.getString("rtmpServerUrlDual");
            Matrice210Parameters.apiWSURL = json.getString("apiWSURL");
            Matrice210Parameters.maxFlightHeight = json.getInt("maxFlightHeight");
            Matrice210Parameters.autoFlightSpeed = json.getInt("autoFlightSpeed");
            Matrice210Parameters.goHomeHeight = json.getInt("goHomeHeight");
            Matrice210Parameters.batteryThreshold = json.getInt("batteryThreshold");
            Matrice210Parameters.thermalDDE = json.getInt("thermalDDE");
            Matrice210Parameters.thermalACE = json.getInt("thermalACE");
            Matrice210Parameters.thermalSSO = json.getInt("thermalSSO");
            Matrice210Parameters.isothermT1 = json.getInt("isothermT1");
            Matrice210Parameters.isothermT2 = json.getInt("isothermT2");
            Matrice210Parameters.isothermT3 = json.getInt("isothermT3");

        } catch (JSONException e) {
            Matrice210Application.writeToErrFile("could override params (json parse)"+ e.toString());
            Log.v("params" , "couldnt override" + e.toString());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection);
        mHandler = new Handler(Looper.getMainLooper());
        checkAndRequestPermissions();
        startSDKRegistration();
        initUI();
        Matrice210DataExposal.setupConnectionToAPI();
        JSONObject params = new JSONObject();
        params = readParamsFile();
        Log.v("params: ", params.toString());
        // Register the broadcast receiver for receiving the device connection's changes.
        IntentFilter filter = new IntentFilter();
        filter.addAction(Matrice210Parameters.FLAG_CONNECTION_CHANGE);
        registerReceiver(mReceiver, filter);
    }

    private void checkAndRequestPermissions() {
        for (String eachPermission : REQUIRED_PERMISSION_LIST) {
            if (ContextCompat.checkSelfPermission(this, eachPermission) != PackageManager.PERMISSION_GRANTED) {
                missingPermission.add(eachPermission);
            }
        }
        if (!missingPermission.isEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    missingPermission.toArray(new String[missingPermission.size()]),
                    REQUEST_PERMISSION_CODE);
        }
    }


    private void startSDKRegistration() {
        if (isRegistrationInProgress.compareAndSet(false, true)) {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {

                    DJISDKManager.getInstance().registerApp(getApplicationContext(), new DJISDKManager.SDKManagerCallback() {
                        @Override
                        public void onInitProcess(DJISDKInitEvent event, final int num) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    final String initProccStr = "Init process:" + String.valueOf(num) + "%";
                                    mTextInitProcess.setText(initProccStr);
                                }
                            });
                        }
                        @Override
                        public void onRegister(DJIError djiError) {
                            if (djiError == DJISDKError.REGISTRATION_SUCCESS) {
                                DJISDKManager.getInstance().startConnectionToProduct();
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        mTextSDKInfo.setText("SDK Status: Registered!");
                                        mTextSDKInfo.setBackgroundResource(R.color.colorPrimary);
                                        mTextSDKInfo.setTextColor(getResources().getColor(R.color.colorWhite));
                                    }
                                });
                                showToast("SDK registered!");

                            } else {

                            }
                            Log.v(TAG, djiError.getDescription());
                        }
                        @Override
                        public void onProductConnect(BaseProduct baseProduct) {
                            // RC Connect probably
                            notifyStatusChange();
                        }
                        @Override
                        public void onProductDisconnect() {
                            componentsConnected = 0;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mTextConnectionStatus.setText("UAV Status: UAV Disconnected!");
                                    mTextConnectionStatus.setBackgroundResource(R.color.colorAccent);
                                    mTextConnectionStatus.setTextColor(getResources().getColor(R.color.colorWhite));
                                    mCompoConnetedStatus.setText("Components Connected: " + String.valueOf(componentsConnected));
                                    mBtnOpen.setEnabled(false);
                                }
                            });
                            showToast("Product disconnected!");
                            notifyStatusChange();
                        }
                        @Override
                        public void onComponentChange(final BaseProduct.ComponentKey componentKey, BaseComponent oldComponent,
                                                      BaseComponent newComponent) {
                            if (newComponent != null) {
                                componentsConnected += 1; //first connection
                                Log.v("component", componentKey.toString());
                                newComponent.setComponentListener(new BaseComponent.ComponentListener() {
                                    final BaseProduct.ComponentKey cp = componentKey;
                                    @Override
                                    public void onConnectivityChange(final boolean isConnected) {
                                        componentsConnected  = (isConnected)? componentsConnected + 1 : componentsConnected - 1;
                                        // hardfixes
                                        if (componentsConnected < 0 ) componentsConnected = 0;
                                        if (Matrice210Getters.getProductInstance() != null &&
                                                Matrice210Getters.getProductInstance() != null &&
                                                Matrice210Getters.getRC() != null &&
                                                Matrice210Getters.getRC().isConnected()) {
                                            componentsConnected += 1;
                                        } else {
                                            //componentsConnected = 0;
                                        }
                                        if (Matrice210Getters.getProductInstance() == null) {
                                            componentsConnected = 0;
                                        }
                                        notifyStatusChange();
                                    }
                                });
                                if (componentsConnected < Matrice210Parameters.componentsDesired) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            mTextConnectionStatus.setText("UAV Status: Waiting all components to connect..");
                                            mTextConnectionStatus.setBackgroundResource(R.color.colorAccent);
                                            mTextConnectionStatus.setTextColor(getResources().getColor(R.color.colorWhite));
                                            mBtnOpen.setEnabled(false);
                                            mCompoConnetedStatus.setText("Components Connected: " + String.valueOf(componentsConnected));
                                        }
                                    });
                                } else {
                                    if (Matrice210Getters.getSDKManager().hasSDKRegistered()) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mTextConnectionStatus.setText("UAV Status: All components connected!");
                                                mTextConnectionStatus.setBackgroundResource(R.color.colorPrimary);
                                                mTextConnectionStatus.setTextColor(getResources().getColor(R.color.colorWhite));
                                                mBtnOpen.setEnabled(true);

                                                Intent intent = new Intent(getApplicationContext(), FlightActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                                                startActivityIfNeeded(intent, 0);
                                            }
                                        });
                                        if (Matrice210Application.initialized == 0)
                                            Matrice210Application.initFunctions();
                                    } else {
                                        showToast("SDK has not yet registered, tap M210 button");
                                    }
                                }
                            }

                            notifyStatusChange();
                        }
                    });
                }
            });
        }
    }

    private void notifyStatusChange() {
        mHandler.removeCallbacks(updateRunnable);
        mHandler.postDelayed(updateRunnable, 500);
    }

    private Runnable updateRunnable = new Runnable() {

        @Override
        public void run() {
            Intent intent = new Intent(Matrice210Parameters.FLAG_CONNECTION_CHANGE);
            getApplicationContext().sendBroadcast(intent);
        }
    };

    @Override
    public void onResume() {
        Log.e(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.e(TAG, "onStop");
        super.onStop();
    }

    public void onReturn(View view){
        Log.e(TAG, "onReturn");
        //this.finish();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG, "onDestroy");

        if (FlightActivity.instance != null) FlightActivity.instance.finish();
        unregisterReceiver(mReceiver);
        super.onDestroy();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void initUI() {
        mTextConnectionStatus = (TextView) findViewById(R.id.text_connection_status);
        mTextInitProcess = (TextView) findViewById(R.id.init_process);
        mCompoConnetedStatus = (TextView) findViewById(R.id.text_components_connected);
        mTextSDKInfo = (TextView) findViewById(R.id.sdk_registration_info);
        mBtnOpen = (Button) findViewById(R.id.btn_open);
        mBtnOpen.setOnClickListener(this);
        mBtnOpen.setEnabled(false);
        mBtnClose = (Button) findViewById(R.id.btn_close);
        mBtnClose.setOnClickListener(this);
        mBtnClose.setEnabled(true);
        mTextSDKInfo.setBackgroundResource(R.color.colorAccent);
        mTextConnectionStatus.setBackgroundResource(R.color.colorAccent);
        mVersionTv = (TextView) findViewById(R.id.sdk_version);
    }

    private void refreshSDKRelativeUI() {
        BaseProduct mProduct = Matrice210Getters.getProductInstance();
        if (null != mProduct && mProduct.isConnected()) {
            Log.v(TAG, "refreshSDK: True");
            final String str = mProduct instanceof Aircraft ? "DJIAircraft" : "DJIHandHeld";
            final String message ="Init process: " + str + " connected";
            if (null != mProduct.getModel()) {
                mTextSDKInfo.setText("SDK Status: Registered!");
            } else {
            //    mTextSDKInfo.setText(R.string.sdk_registration_info);
            }
        } else {
            Log.v(TAG, "refreshSDK: False");
            mBtnOpen.setEnabled(false);
            mTextSDKInfo.setText(R.string.sdk_registration_info);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_open: {
                Intent intent = new Intent(this, FlightActivity.class);
                startActivityIfNeeded(intent, 0);
            }
            case R.id.btn_close: {
                finish();
                break;
            }
            default:
                break;
        }
    }

    private void showToast(final String toastMsg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_LONG).show();
            }
        });
    }
}
