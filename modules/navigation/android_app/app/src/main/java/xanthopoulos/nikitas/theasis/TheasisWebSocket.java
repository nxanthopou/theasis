/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */

package xanthopoulos.nikitas.theasis;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

import tech.gusavila92.websocketclient.WebSocketClient;


public class TheasisWebSocket {
    protected static boolean isConnected = false;
    private static WebSocketClient webSocketClient = null;

    protected static void createWebSocketClient() {
        URI uri;
        try {
            uri = new URI(Matrice210Parameters.apiWSURL);
            webSocketClient = webSocketCli(uri);
            webSocketClient.setConnectTimeout(Matrice210Parameters.webSocketConnectionTimeout);
            webSocketClient.setReadTimeout(Matrice210Parameters.webSocketReadTimeout);
            webSocketClient.enableAutomaticReconnection(Matrice210Parameters.webSocketAutoReconnectionTimeout);
            webSocketClient.connect();
        } catch (java.net.URISyntaxException e){
            Log.v("createWSCli", e.toString());
        }
    }

    private static WebSocketClient webSocketCli(URI uri) {
        WebSocketClient cli = new WebSocketClient(uri) {
            @Override
            public void onOpen() {
                isConnected = true;
            }

            @Override
            public void onTextReceived(String message) {
                missionStartEvent(message);
            }

            @Override
            public void onBinaryReceived(byte[] data) {

            }

            @Override
            public void onPingReceived(byte[] data) {
                isConnected = true;
                webSocketClient.sendPong(data);
            }

            @Override
            public void onPongReceived(byte[] data) {
            }

            @Override
            public void onException(Exception e) {
                if (e.getMessage() != null) Log.v("WebSocketException", e.getMessage());
                if (e instanceof java.net.ConnectException) isConnected = false;
            }

            @Override
            public void onCloseReceived() {
                isConnected = false;
            }
        };
        return cli;
    }

    private static void missionStartEvent(String message) {
        try {
            JSONObject parsedMessage = new JSONObject(message);
            String routeStr = parsedMessage.get("route").toString();
            String autoFlightSpeedStr = parsedMessage.get("flight_speed").toString();
            float autoFlightSpeed = Float.parseFloat(autoFlightSpeedStr);
            JSONArray waypointArr = new JSONArray(routeStr);
            int routeLength = waypointArr.length();
            double[][] waypointsPointer = new double[routeLength][3];

            for (int i=0; i<routeLength; i++) {
                JSONArray pointInfo = new JSONArray(waypointArr.get(i).toString());
                String latStr = pointInfo.get(0).toString();
                String lonStr = pointInfo.get(1).toString();
                String altStr = pointInfo.get(2).toString();
                Double latitude = Double.parseDouble(latStr);
                Double longitude = Double.parseDouble(lonStr);
                Double altitude = Double.parseDouble(altStr);
                waypointsPointer[i][0] = latitude;
                waypointsPointer[i][1] = longitude;
                waypointsPointer[i][2] = altitude - (double)
                        Matrice210Application.absAltitude; // relative, set by system's parameters
            }

            Matrice210Application.writeToLogFile(routeStr);
            Matrice210Mission.initiatePPMission(waypointsPointer, autoFlightSpeed);
        } catch (JSONException e) {
            Matrice210Application.writeToErrFile("JSON Exception, onTextReceived: " + e.toString());
            Log.v("WebSocket", "textReceived: " + e.toString());
        }
    }

    protected static void sendTheasis(JSONObject jsonData) {
        if (webSocketClient != null) webSocketClient.send(jsonData.toString());
    }

}