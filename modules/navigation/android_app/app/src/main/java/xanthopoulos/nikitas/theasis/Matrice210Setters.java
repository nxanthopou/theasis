/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */

package xanthopoulos.nikitas.theasis;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.List;

import dji.common.battery.BatteryState;
import dji.common.camera.SettingsDefinitions;
import dji.common.error.DJIError;
import dji.common.flightcontroller.ConnectionFailSafeBehavior;
import dji.common.flightcontroller.FlightControllerState;
import dji.common.flightcontroller.LocationCoordinate3D;
import dji.common.flightcontroller.ObstacleDetectionSector;
import dji.common.flightcontroller.VisionDetectionState;
import dji.common.gimbal.Axis;
import dji.common.gimbal.GimbalMode;
import dji.common.gimbal.GimbalState;
import dji.common.mission.waypoint.WaypointMissionDownloadEvent;
import dji.common.mission.waypoint.WaypointMissionExecutionEvent;
import dji.common.mission.waypoint.WaypointMissionUploadEvent;
import dji.common.model.LocationCoordinate2D;
import dji.common.remotecontroller.ChargeRemaining;
import dji.common.util.CommonCallbacks;
import dji.midware.usb.P3.UsbAccessoryService;
import dji.sdk.airlink.LightbridgeLink;
import dji.sdk.base.DJIDiagnostics;
import dji.sdk.camera.Camera;
import dji.sdk.camera.VideoFeeder;
import dji.sdk.mission.waypoint.WaypointMissionOperatorListener;

public class Matrice210Setters {
    protected static void setupCameras() {
        List<Camera> cameras = Matrice210Getters.getCameras();
        for (Camera camera : cameras) {
            if (camera.getDisplayName() == Camera.DisplayNameX5S) {
                Matrice210Setters.setupRGBCamera(camera);
            } else {
                Matrice210Setters.setupThermalCamera(camera);
            }
        }

    }

    protected static void setRCBatteryCallback() {
        Matrice210Getters.getRC().setChargeRemainingCallback(new ChargeRemaining.Callback() {
            @Override
            public void onUpdate(@NonNull ChargeRemaining chargeRemaining) {
                Matrice210Application.rcBatteryLifeLeft = chargeRemaining.getRemainingChargeInPercent();
            }
        });
    }

    protected static void setBatteryCallback() {
        Matrice210Getters.getBattery().setStateCallback(new BatteryState.Callback() {
            @Override
            public void onUpdate(BatteryState batteryState) {
                Matrice210Application.batteryLifeLeft = batteryState.getChargeRemainingInPercent();
            }
        });
    }

    protected static void setupAutonomousFunctions() {
        Matrice210Setters.setGoHomeHeight();
        Matrice210Setters.setMultipleFlightModes();
        Matrice210Setters.setConnectionFailsafe();
        Matrice210Setters.setFlightLimitations(); // no radius limit, height from params
    }

    protected static void setFlightLimitations() {
        //if RadiusLimitationEnabled == true, add setMaxFlightRadius() accordingly
        Matrice210Getters.getFlightController().setMaxFlightRadiusLimitationEnabled(false, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "flight limitation");
            }
        });
        Matrice210Getters.getFlightController().setMaxFlightHeight(Matrice210Parameters.maxFlightHeight, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "max flight height");
            }
        });
    }

    protected static void setGimbalSpin() {
        Matrice210Getters.getFlightController().setAutoQuickSpinEnabled(Matrice210Parameters.autoGimbalSpin, null);
    }

    protected static void setGoHomeHeight() {
        Matrice210Getters.getFlightController().setGoHomeHeightInMeters(Matrice210Parameters.goHomeHeight, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set go Home height");
            }
        });
    }

    protected static void setMultipleFlightModes() {
        Matrice210Getters.getFlightController().setMultipleFlightModeEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set multiple flight mode");
            }
        });
    }

    protected static void setConnectionFailsafe() {
        Matrice210Getters.getFlightController().setConnectionFailSafeBehavior(ConnectionFailSafeBehavior.GO_HOME, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "connection failsafe");
            }
        });
    }

    protected static void setFlightControllerCallback() {
        Matrice210Getters.getFlightController().setStateCallback(new FlightControllerState.Callback() {
            @Override
            public void onUpdate(@NonNull FlightControllerState flightControllerState) {
                final LocationCoordinate3D currentLocation = flightControllerState.getAircraftLocation();

                // IMU/Barometer data, return NaN sometimes
                final String tmpLatitude = String.valueOf(currentLocation.getLatitude());
                final String tmpLongitude = String.valueOf(currentLocation.getLongitude());
                final String tmpAltitude = String.valueOf(currentLocation.getAltitude());
                final String tmpAbsAltitude = String.valueOf(flightControllerState.getTakeoffLocationAltitude());

                Matrice210Application.satelliteCount = flightControllerState.getSatelliteCount();

                Matrice210Application.gpsSignalLevel = flightControllerState.getGPSSignalLevel();
                Matrice210Application.windWarning = flightControllerState.getFlightWindWarning();
                Matrice210Application.roll = flightControllerState.getAttitude().roll;
                Matrice210Application.pitch = flightControllerState.getAttitude().pitch;
                Matrice210Application.yaw = flightControllerState.getAttitude().yaw;

                Matrice210Application.latitude = currentLocation.getLatitude();
                Matrice210Application.longitude = currentLocation.getLongitude();
                Matrice210Application.relAltitude = currentLocation.getAltitude();

                Matrice210Application.latitude = (tmpLatitude == "NaN")?
                        -1.0 :
                        Matrice210Application.latitude;
                Matrice210Application.longitude = (tmpLongitude == "NaN")?
                        -1.0 :
                        Matrice210Application.longitude;
                Matrice210Application.relAltitude = (tmpAltitude == "NaN")?
                        -1.0f :
                        Matrice210Application.relAltitude;
                Matrice210Application.absAltitude = (tmpAbsAltitude == "NaN")?
                        -1.0f :
                        Math.abs(Float.parseFloat(tmpAbsAltitude));

            }
        });
    }

    // https://developer.dji.com/api-reference/android-api/Components/IntelligentFlightAssistant/DJIIntelligentFlightAssistant.html
    // for full allowed functionalities.
    protected static void setFlightAssistant() {
        // Enable collision avoidance
        Matrice210Getters.getAssistant().setCollisionAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set collision avoidance enabled");
            }
        });
        // Enable upwards avoidance.
        Matrice210Getters.getAssistant().setUpwardsAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set upwards avoidance enabled");
            }
        });
        // Enables Remote Obstacle Avoidance during RTH.
        Matrice210Getters.getAssistant().setRTHRemoteObstacleAvoidanceEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set rth remote obstacle avoidance enabled");
            }
        });
        // Enables/disables precision landing for RTH using Vision sensors.
        Matrice210Getters.getAssistant().setPrecisionLandingEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set precision landing");
            }
        });
        // Enables/disables landing protection. During auto-landing,
        // the downwards facing vision sensor will check if the ground
        // surface is flat enough for a safe landing. If it is not and
        // landing protection is enabled, then landing will abort and need
        // to be manually performed by the user.
        Matrice210Getters.getAssistant().setLandingProtectionEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set landing protection");
            }
        });
        //remaining batt enough only for go Home
        Matrice210Getters.getFlightController().setSmartReturnToHomeEnabled(true, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set smart return home");
            }
        });

    }

    //to be rewritten
    //https://developer.dji.com/api-reference/android-api/Components/VisionDetectionState/DJIVisionDetectionState.html#djivisiondetectionstate
    protected static void setForwardObstacleAvoidanceData() {
        Matrice210Getters.getAssistant().setVisionDetectionStateUpdatedCallback(new VisionDetectionState.Callback() {
            @Override
            public void onUpdate(@NonNull VisionDetectionState visionDetectionState) {
                ObstacleDetectionSector[] visionSystemSectors = visionDetectionState.getDetectionSectors();
                int visionSystemSectorCounter = 0;
                for (ObstacleDetectionSector visionDetectionSector : visionSystemSectors) {
                    visionDetectionSector.getWarningLevel();
                    //String obstacleDistance = String.valueOf(visionDetectionSector.getObstacleDistanceInMeters());
                    //if (obstacleDistance == "NaN") obstacleDistance = "50";
                    //setDataTXMessage("VSS" + String.valueOf(visionSystemSectorCounter), obstacleDistance);
                    visionSystemSectorCounter += 1;
                }
            }
        });
    }


    //https://developer.dji.com/api-reference/android-api/Components/Diagnostics/DJIDiagnostics.html#djidiagnostics_djidiagnosticserror_inline
    protected static void setDiagnosticsCallback() {
        Matrice210Getters.getProductInstance().setDiagnosticsInformationCallback(new DJIDiagnostics.DiagnosticsInformationCallback() {
            @Override
            public void onUpdate(List<DJIDiagnostics> diagnosticsList) {
                Matrice210Application.diagnostics = diagnosticsList;
                if (!diagnosticsList.isEmpty()) {
                    for (DJIDiagnostics j : diagnosticsList) {
                        Matrice210Application.writeToLogFile(String.valueOf(j.getCode()) + " -> " + j.getReason() + ": " + j.getSolution());
                        //8012 -> IMU is initializing. Do not move the aircraft until initialization is complete:
                    }
                }
            }
        });
    }

    protected static void setCameraBWSettings() {
        LightbridgeLink lbLink = Matrice210Getters.getAirlink().getLightbridgeLink();
        lbLink.setBandwidthAllocationForMainCamera((float) 1.0, null);
        lbLink.setBandwidthAllocationForLeftCamera((float) 0.8, null);
    }

    //not used?
    protected static void setupTheasisDataConnection() {
        Matrice210DataExposal.setupAPIMessage();
        Matrice210DataExposal.setupConnectionToAPI();
        Matrice210Application.informAPITimerInit();
    }

    protected static void setGimbalCallback() {
        Matrice210Getters.getGimbal().
        setStateCallback(new GimbalState.Callback() {
            @Override
            public void onUpdate (@NonNull GimbalState gimbalState){
                Matrice210Application.gimbalRoll = gimbalState.getAttitudeInDegrees().getRoll();
                Matrice210Application.gimbalPitch = gimbalState.getAttitudeInDegrees().getPitch();
                Matrice210Application.gimbalYaw = gimbalState.getAttitudeInDegrees().getYaw();
            }
        });
    }

    protected static void setupGimbal() {
        Matrice210Setters.setGimbalSpin();
        Matrice210Getters.getGimbal().getAttitudeSynchronizationEnabled(Axis.YAW, new CommonCallbacks.CompletionCallbackWith<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (aBoolean == false) Matrice210Setters.setSynchronizedGimbal(true);
                Matrice210Getters.getGimbal().setComponentIndex(1);
                Matrice210Setters.setGimbalMode(GimbalMode.FREE);
                Matrice210Getters.getGimbal().setComponentIndex(0);
                Matrice210Setters.setGimbalMode(GimbalMode.FREE);
                Matrice210Setters.setGimbalCallback();
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "setupGimbal()");
            }
        });
    }

    protected static void setGimbalMode(GimbalMode gimbalMode) {
        Matrice210Getters.getGimbal().setMode(gimbalMode, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set gimbal mode");
            }
        });
    }

    protected static void setSynchronizedGimbal(boolean synced) {
        Matrice210Getters.getGimbal().getAttitudeSynchronizationEnabled(Axis.YAW, new CommonCallbacks.CompletionCallbackWith<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (aBoolean == false ) {
                    Matrice210Getters.getGimbal().setAttitudeSynchronizationEnabled(true, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            Matrice210Application.generalErrorHandling(djiError, "gimbal set synchronized");
                        }
                    });
                }
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "get synchronized gimbal");
            }
        });
    }

    protected static void setupRGBCamera(final Camera camera) {
        camera.getFocusMode(new CommonCallbacks.CompletionCallbackWith<SettingsDefinitions.FocusMode>() {
            @Override
            public void onSuccess(SettingsDefinitions.FocusMode focusMode) {
                if (focusMode != Matrice210Parameters.cameraFocusMode) {
                    camera.setFocusMode(Matrice210Parameters.cameraFocusMode, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            Matrice210Application.generalErrorHandling(djiError, "on setting camera focus mode");
                        }
                    });
                }
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "get rgb focus mode");
            }
        });
    }

    protected static void setupThermalCamera(final Camera camera) {
        camera.getThermalPalette(new CommonCallbacks.CompletionCallbackWith<SettingsDefinitions.ThermalPalette>() {
            @Override
            public void onSuccess(SettingsDefinitions.ThermalPalette thermalPalette) {
                if (thermalPalette != Matrice210Parameters.thermalCameraColorPalette) {
                    camera.setThermalPalette(Matrice210Parameters.thermalCameraColorPalette, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            Matrice210Application.generalErrorHandling(djiError, "set thermal palette");
                        }
                    });
                }
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "get thermal palette");
            }
        });


        camera.setThermalIsothermUnit(Matrice210Parameters.thermalISOUnit, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set thermal isotherm unit");
            }
        });

        camera.getThermalIsothermUnit(new CommonCallbacks.CompletionCallbackWith<SettingsDefinitions.ThermalIsothermUnit>() {
            @Override
            public void onSuccess(SettingsDefinitions.ThermalIsothermUnit thermalIsothermUnit) {
                Log.v("isotherm unit", thermalIsothermUnit.toString());
            }

            @Override
            public void onFailure(DJIError djiError) {
                Log.v("isotherm unit", "could not get");
            }
        });

        camera.getThermalIsothermEnabled(new CommonCallbacks.CompletionCallbackWith<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                Log.v("isotherm", aBoolean.toString());
                if (aBoolean != Matrice210Parameters.thermalIsothermEnabled) {
                    camera.setThermalIsothermEnabled(Matrice210Parameters.thermalIsothermEnabled, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            Matrice210Application.generalErrorHandling(djiError, "set thermal isotherm");
                        }
                    });
                }
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "get thermal isotherm");
            }
        });

        camera.setThermalScene(Matrice210Parameters.thermalScene, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set thermal scene");
            }
        });
        camera.setThermalIsothermEnabled(Matrice210Parameters.thermalIsothermEnabled, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set thermal isotherm");

                camera.setThermalIsothermLowerValue(Matrice210Parameters.isothermT1, new CommonCallbacks.CompletionCallback() {
                    @Override
                    public void onResult(DJIError djiError) {
                        Matrice210Application.generalErrorHandling(djiError, "set isotherm lower theshold");
                        camera.setThermalIsothermMiddleValue(Matrice210Parameters.isothermT2, new CommonCallbacks.CompletionCallback() {
                            @Override
                            public void onResult(DJIError djiError) {
                                Matrice210Application.generalErrorHandling(djiError, "set isotherm middle theshold");

                                camera.setThermalIsothermUpperValue(Matrice210Parameters.isothermT3, new CommonCallbacks.CompletionCallback() {
                                    @Override
                                    public void onResult(DJIError djiError) {
                                        Matrice210Application.generalErrorHandling(djiError, "set isotherm upper theshold");
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        camera.getThermalGainMode(new CommonCallbacks.CompletionCallbackWith<SettingsDefinitions.ThermalGainMode>() {
            @Override
            public void onSuccess(SettingsDefinitions.ThermalGainMode thermalGainMode) {
                if (thermalGainMode != Matrice210Parameters.thermalGainMode) {
                    camera.setThermalGainMode(Matrice210Parameters.thermalGainMode, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            Matrice210Application.generalErrorHandling(djiError, "set thermal gain mode");
                        }
                    });
                }
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "get thermal gain mode");
            }
        });

        camera.getThermalScene(new CommonCallbacks.CompletionCallbackWith<SettingsDefinitions.ThermalScene>() {
            @Override
            public void onSuccess(SettingsDefinitions.ThermalScene thermalScene) {
                if (thermalScene != SettingsDefinitions.ThermalScene.PROFILE_1) {
                    camera.setThermalScene(SettingsDefinitions.ThermalScene.PROFILE_1, new CommonCallbacks.CompletionCallback() {
                        @Override
                        public void onResult(DJIError djiError) {
                            camera.setThermalDDE(Matrice210Parameters.thermalDDE, new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    Matrice210Application.generalErrorHandling(djiError, "set thermal DDE");
                                }
                            });
                            camera.setThermalACE(Matrice210Parameters.thermalACE, new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    Matrice210Application.generalErrorHandling(djiError, "set thermal ACE");
                                }
                            });
                            camera.setThermalSSO(Matrice210Parameters.thermalSSO, new CommonCallbacks.CompletionCallback() {
                                @Override
                                public void onResult(DJIError djiError) {
                                    Matrice210Application.generalErrorHandling(djiError, "set thermal SSO");
                                }
                            });
                        }
                    });
                }
            }
            @Override
            public void onFailure(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "get thermal scene");
            }
        });

    }


    protected static void setupVideoFeeds() {
        VideoFeeder videoFeeder = VideoFeeder.getInstance();

        VideoFeeder.VideoDataListener mReceivedVideoDataListener1 = new VideoFeeder.VideoDataListener() {
            @Override
            public void onReceive(byte[] videoBuffer, int size) {

                if (Matrice210Application.codecManagerRGB != null) {
                    Matrice210Application.codecManagerRGB.sendDataToDecoder(videoBuffer, size, UsbAccessoryService.VideoStreamSource.Camera.getIndex());
                }
            }
        };

        // SHOULD BE SecondCamera.getIndex()
        VideoFeeder.VideoDataListener mReceivedVideoDataListener2 = new VideoFeeder.VideoDataListener() {
            @Override
            public void onReceive(byte[] videoBuffer, int size) {
                if (Matrice210Application.codecManagerThermal != null) {
                    Matrice210Application.codecManagerThermal.sendDataToDecoder(videoBuffer, size, UsbAccessoryService.VideoStreamSource.SecondaryCamera.getIndex());
                }
            }
        };

        videoFeeder.getPrimaryVideoFeed().addVideoDataListener(mReceivedVideoDataListener1);
        videoFeeder.getSecondaryVideoFeed().addVideoDataListener(mReceivedVideoDataListener2);
        videoFeeder.setTranscodingDataRate(Matrice210Parameters.transcodingRate);
    }


    protected static void setFlightSpeed() {
        Matrice210Getters.getWaypointMissionOperator().setAutoFlightSpeed(Matrice210Parameters.autoFlightSpeed, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Log.v("AutoFlightSpeed", djiError.getDescription());
            }
        });
    }

    protected static void setHomeLocationCurrent() {
        Matrice210Getters.getFlightController().setHomeLocationUsingAircraftCurrentLocation(new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                Matrice210Application.generalErrorHandling(djiError, "set home current loc");
                if (djiError != null) {
                    Matrice210Getters.getHomeLocation();
                }
            }
        });
    }

    protected static void setHomeLocation(double homeLatitude, double homeLongitude) {
        LocationCoordinate2D curLoc = new LocationCoordinate2D(homeLatitude, homeLongitude);

        Matrice210Getters.getFlightController().setHomeLocation(curLoc, new CommonCallbacks.CompletionCallback() {
            @Override
            public void onResult(DJIError djiError) {
                if (djiError != null) {
                    Matrice210Application.generalErrorHandling(djiError, "set home");
                }
            }
        });
    }

    protected static void setWaypointMissionOperatorListener() {
        Matrice210Getters.getWaypointMissionOperator().addListener(new WaypointMissionOperatorListener() {
            @Override
            public void onDownloadUpdate(@NonNull WaypointMissionDownloadEvent waypointMissionDownloadEvent) {
                Log.v("Mission DownloadUpdate", waypointMissionDownloadEvent.toString());
            }
            @Override
            public void onUploadUpdate(@NonNull WaypointMissionUploadEvent waypointMissionUploadEvent) {
                Log.v("Mission UploadUpdate", waypointMissionUploadEvent.toString());
            }
            @Override
            public void onExecutionUpdate(@NonNull WaypointMissionExecutionEvent waypointMissionExecutionEvent) {
                Log.v("Mission ExecutionUpdate", waypointMissionExecutionEvent.toString());
            }
            @Override
            public void onExecutionStart() {
                Log.v("Mission ExecutionStart", "Exec started");
                Matrice210Application.writeToLogFile("execution start, setWaypointMissionOperatorListener");
                Matrice210DataExposal.setKey("onMission", "true");
            }
            @Override
            public void onExecutionFinish(@Nullable DJIError djiError) {
                if (djiError != null) Log.v("Mission ExecutionFinish", djiError.getDescription());
                Matrice210Application.writeToLogFile("execution finish, setWaypointMissionOperatorListener");
                Matrice210Application.generalErrorHandling(djiError, "execution finished, setWaypointMissionOperatorListener");

                Matrice210DataExposal.setKey("onMission", "false");
            }
        });
    }
}
