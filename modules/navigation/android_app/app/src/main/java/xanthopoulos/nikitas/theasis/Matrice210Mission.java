/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */
 
package xanthopoulos.nikitas.theasis;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import dji.common.error.DJIError;
import dji.common.mission.waypoint.Waypoint;
import dji.common.mission.waypoint.WaypointAction;
import dji.common.mission.waypoint.WaypointActionType;
import dji.common.mission.waypoint.WaypointMission;
import dji.common.mission.waypoint.WaypointMissionState;
import dji.common.util.CommonCallbacks;
import dji.sdk.mission.MissionControl;
import dji.sdk.mission.timeline.triggers.BatteryPowerLevelTrigger;
import dji.sdk.mission.timeline.triggers.Trigger;
import dji.sdk.mission.timeline.triggers.WaypointReachedTrigger;

public class Matrice210Mission {
    protected static void initiatePPMission(final double[][] waypointsPointer, float flightSpeed) {
        final int numOfWaypoints = waypointsPointer.length;
        final double targetLat = waypointsPointer[numOfWaypoints-1][0];
        final double targetLon = waypointsPointer[numOfWaypoints-1][1];

        Matrice210Parameters.autoFlightSpeed = flightSpeed;
        Matrice210Application.visibleErrorsStr = null;

//        Matrice210Mission.stopMission();
        Matrice210Setters.setHomeLocationCurrent();
        Matrice210Setters.setFlightSpeed();
        Matrice210Application.rotateGimbal(0f, -10f, 0f, 0.1d, null);

        WaypointMission mission = buildWaypointMission(waypointsPointer);
        DJIError missionLoadErr = Matrice210Getters.getWaypointMissionOperator().loadMission(mission);
        Matrice210DataExposal.startStream();
        //Matrice210Application.switchStreamTimerInit();
        
        if (missionLoadErr == null ) {
            Matrice210Getters.getWaypointMissionOperator().uploadMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError uploadError) {
                    if (uploadError == null) {
                        Matrice210Application.writeToLogFile("mission start requested, everything ok..");
                        Matrice210Mission.startMission();
                    } else {
                        Matrice210Application.generalErrorHandling(uploadError, "uploadMissionErr");
                        Matrice210Getters.getWaypointMissionOperator().retryUploadMission(new CommonCallbacks.CompletionCallback() {
                            @Override
                            public void onResult(DJIError retryUploadError) {

                                if (retryUploadError == null) {
                                    Matrice210Application.writeToLogFile("mission start requested on reupload, everything ok..");
                                    Matrice210Mission.startMission();
                                } else {
                                    Matrice210Application.writeToErrFile("could not reupload mission ");
                                }
                            }
                        });
                    }
                }
            });
        } else {
            Matrice210Application.generalErrorHandling(missionLoadErr, "initiateMissionLoad");
        }
    }

    // [latitude, longitude]
    protected static WaypointMission buildWaypointMission(double[][] waypointsMatrix) {
        List<Waypoint> waypointsList = new ArrayList<>();
        int waypointsCount = waypointsMatrix.length;
        double alarmLatitude = waypointsMatrix[waypointsCount-1][0];
        double alarmLongitude = waypointsMatrix[waypointsCount-1][1];
        float alarmAltitude =  (float)waypointsMatrix[waypointsCount-1][2];

        WaypointMission.Builder wpBuilder = new WaypointMission.Builder()
                .autoFlightSpeed(Matrice210Parameters.autoFlightSpeed)
                .maxFlightSpeed(Matrice210Parameters.maxFlightSpeed)
                .setExitMissionOnRCSignalLostEnabled(Matrice210Parameters.quitMissionOnRCLose)
                .finishedAction(Matrice210Parameters.finishAction)
                .flightPathMode(Matrice210Parameters.flightPathMode)
                .gotoFirstWaypointMode(Matrice210Parameters.waypointGoToMode)
                .headingMode(Matrice210Parameters.waypointHeadingMode)
                .repeatTimes(0); // single shot

        for (int wpCounter=0; wpCounter < waypointsCount; wpCounter++) {
            double wpLat = waypointsMatrix[wpCounter][0];
            double wpLon = waypointsMatrix[wpCounter][1];
            float wpAlt = (float)waypointsMatrix[wpCounter][2];
            Waypoint wp = new Waypoint(wpLat, wpLon, wpAlt);
            wp.addAction(new WaypointAction(WaypointActionType.GIMBAL_PITCH, -90));
            if (wpCounter >= waypointsCount - 7) {
                wp.addAction(new WaypointAction(WaypointActionType.STAY, 30000));
            }
            waypointsList.add(wp);
        }

        Matrice210Mission.setWaypointsTriggers(waypointsList);
        wpBuilder.waypointList(waypointsList).waypointCount(waypointsList.size());

        return wpBuilder.build();
    }

    protected static void startMission() {
        try {
            Thread.sleep(2000); //might not be needed
        } catch (InterruptedException e) {
            Matrice210Application.writeToErrFile("thread sleep inside start mission error");
        }
        if (Matrice210Getters.getMissionState() == WaypointMissionState.READY_TO_EXECUTE) {
            Matrice210Getters.getWaypointMissionOperator().startMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Matrice210Application.generalErrorHandling(djiError, "start mission");
                    if (djiError != null) Matrice210DataExposal.setKey("onMission", "true");
                }
            });
        } else {
            Matrice210Application.writeToLogFile("start mission requested, although waypoint mission state is " + Matrice210Getters.getMissionState().toString());
        }
    }

    protected static void pauseMission() {
        if (Matrice210Getters.getMissionState() == WaypointMissionState.EXECUTING)
            Matrice210Getters.getWaypointMissionOperator().pauseMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Matrice210Application.generalErrorHandling(djiError, "pause mission");
                }
            });
    }

    protected static void resumeMission() {
        if (Matrice210Getters.getMissionState() == WaypointMissionState.EXECUTION_PAUSED)
            Matrice210Getters.getWaypointMissionOperator().resumeMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Matrice210Application.generalErrorHandling(djiError, "resume mission");
                }
            });
    }

    protected static void stopMission() {
        WaypointMissionState missionState = Matrice210Getters.getMissionState();
        if (missionState == WaypointMissionState.EXECUTING || missionState == WaypointMissionState.EXECUTION_PAUSED)
            Matrice210Getters.getWaypointMissionOperator().stopMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Matrice210Application.generalErrorHandling(djiError, "stop mission");
                    Matrice210DataExposal.setKey("onMission", "false");
                }
            });
    }

    protected static void downloadMission() {
        WaypointMissionState missionState = Matrice210Getters.getMissionState();
        if (missionState == WaypointMissionState.EXECUTING || missionState == WaypointMissionState.EXECUTION_PAUSED)
            Matrice210Getters.getWaypointMissionOperator().downloadMission(new CommonCallbacks.CompletionCallback() {
                @Override
                public void onResult(DJIError djiError) {
                    Matrice210Application.generalErrorHandling(djiError, "download mission");
                }
            });
    }


    /****************************************************/
    /*                  Triggers                        */
    /****************************************************/
    // rotates camera on prelast waypoint, sets message's mission off on last
    protected static void setWaypointsTriggers(final List<Waypoint> waypointsList) {
         int size = waypointsList.toArray().length;
		 for (int i=0; i<size; i++) {
			 WaypointReachedTrigger wpTrigger = new WaypointReachedTrigger();
             wpTrigger.setWaypointIndex(i);
             wpTrigger.setAction(new Trigger.Action() {
                 @Override
                 public void onCall() {
                     if (Matrice210Getters.getLSManager().isStreaming() == false) {
                         Matrice210DataExposal.startStream();
                     }
                     Matrice210Application.rotateGimbal(0f, -90f, 0f, 0.1d, null);
                 }
             });
			 wpTrigger.start();
		 }
    }

    // might need listeners?
    protected static void addBatteryPowerLevelTrigger() {
        MissionControl missionControl = Matrice210Getters.getMissionControl();
        BatteryPowerLevelTrigger batteryTrigger = new BatteryPowerLevelTrigger();


        /**/ // java.lang.NullPointerException: Attempt to invoke interface method 'boolean java.util.List.add(java.lang.Object)' on a null object reference
        List<Trigger> triggers =
                (Matrice210Getters.getMissionControl().getTriggers() == null)? new ArrayList<Trigger>() : Matrice210Getters.getMissionControl().getTriggers();
        batteryTrigger.setPowerPercentageTriggerValue(Matrice210Parameters.batteryThreshold);
        batteryTrigger.setAction(new Trigger.Action() {
            @Override
            public void onCall() {
                Log.v("BatteryLow", String.valueOf(Matrice210Parameters.batteryThreshold));
                // RTH?
            }
        });
        triggers.add(batteryTrigger);
        missionControl.setTriggers(triggers);
    }
}
