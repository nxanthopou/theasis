/*  Developer: Xanthopoulos Nikitas,
 *  2019,
 */

package xanthopoulos.nikitas.theasis;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.sql.Timestamp;

import org.json.JSONObject;

import dji.common.error.DJIError;
import dji.common.flightcontroller.FlightWindWarning;
import dji.common.flightcontroller.GPSSignalLevel;
import dji.common.gimbal.Rotation;
import dji.common.gimbal.RotationMode;
import dji.common.model.LocationCoordinate2D;
import dji.common.util.CommonCallbacks;
import dji.sdk.base.DJIDiagnostics;
import dji.sdk.codec.DJICodecManager;


public class Matrice210Application extends Application {
    private Application instance;

    protected static String visibleErrorsStr = null;
    protected static DJICodecManager codecManagerRGB = null;
    protected static DJICodecManager codecManagerThermal = null;
    protected static LocationCoordinate2D homeLocation = null;
    protected static List<DJIDiagnostics> diagnostics = null; // diagnostics are stored here through setDiagnosticsCallback().
    protected static int satelliteCount = 0;
    protected static double latitude, longitude = 0.0;
    protected static double roll, pitch, yaw = 0.0;
    protected static double gimbalRoll, gimbalPitch, gimbalYaw = 0.0;
    protected static float relAltitude, absAltitude = 0f;
    protected static int batteryLifeLeft, rcBatteryLifeLeft = 0;
    protected static JSONObject apiTXMessage = new JSONObject();
    protected static Timer livestreamTimer= null;
    protected static Timer serverConnectionTimer= null;
    protected static GPSSignalLevel gpsSignalLevel = GPSSignalLevel.NONE;
    protected static FlightWindWarning windWarning = FlightWindWarning.UNKNOWN;
    protected static boolean isOpticalStream = true;
    protected static int initialized = 0;
    protected Matrice210Application() {}

    @Override
    public void onCreate() {
        super.onCreate();
    }


    public void nullifyDJI() {

    }

    /****************************************************/
    /*                 Initializations                  */
    /****************************************************/
    protected static void initFunctions() {
        Matrice210Setters.setFlightAssistant();
        Matrice210Setters.setupCameras();
        Matrice210Setters.setFlightControllerCallback();
        Matrice210Setters.setDiagnosticsCallback();
        Matrice210Setters.setForwardObstacleAvoidanceData();
        Matrice210Setters.setupVideoFeeds();
        Matrice210Setters.setCameraBWSettings();
        Matrice210Setters.setBatteryCallback();
        Matrice210Setters.setRCBatteryCallback();
        Matrice210Setters.setupGimbal();
        Matrice210Setters.setupAutonomousFunctions();
        Matrice210Setters.setWaypointMissionOperatorListener();
        Matrice210DataExposal.startStream();
        Matrice210Application.informAPITimerInit();
        //addBatteryPowerLevelTrigger();
        Matrice210Application.initialized = 1;
    }


    /****************************************************/
    /*                 Gimbal/Cameras                   */
    /****************************************************/
    //resets gimbal mode to FREE for X5S
    protected static void rotateGimbal(float yaw, float pitch, float roll, double time, CommonCallbacks.CompletionCallback callback) {
        Rotation.Builder builder = new Rotation.Builder().mode(RotationMode.ABSOLUTE_ANGLE).time(time);
        builder.yaw(yaw);
        builder.pitch(pitch);
        builder.roll(roll);
        Rotation rotation = builder.build();
        Matrice210Getters.getGimbal().rotate(rotation, callback);
    }


    /****************************************************/
    /*                  Timers                          */
    /****************************************************/
    protected static void informAPITimerInit() {
        serverConnectionTimer = new Timer();
        serverConnectionTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (Matrice210Getters.getProductInstance() != null && Matrice210Getters.getFlightController() != null) {
                    Matrice210DataExposal.setMessageValues();
                } else {
                    Matrice210DataExposal.setupAPIMessage();
                }
                TheasisWebSocket.sendTheasis(apiTXMessage);
            }

        },0, Matrice210Parameters.apiConnectionPeriod); // send message to API every apiConnectionPeriod seconds
    }

    protected static void switchStreamTimerInit() {
        if (livestreamTimer != null) {
            livestreamTimer.cancel();
            livestreamTimer = null;
        }
        if (Matrice210Getters.getLSManager().isStreaming() == false) {
            Matrice210DataExposal.startStream();
        }
        livestreamTimer = new Timer();
        livestreamTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                Matrice210DataExposal.changeLiveStreamSource();
            }

        }, 0, Matrice210Parameters.feedSwitchPeriod); // change video stream source every feedSwitchPeriod seconds

    }


    /****************************************************/
    /*                 Application                      */
    /****************************************************/
    @Override
    public Context getApplicationContext() {
        return instance;
    }

    protected void setContext(Application application) {
        instance = application;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }



    /****************************************************/
    /*                  Errors                          */
    /****************************************************/
    protected static void generalErrorHandling(DJIError err, String additionalInfo) {
        if (err != null) {
            // general errors handling
            final String errDesc = err.getDescription();
            final String error = errDesc + " : " + additionalInfo;
            Log.e("GenerErrHand", error);
            writeToErrFile(error);
            Matrice210Application.visibleErrorsStr =
                    (Matrice210Application.visibleErrorsStr == null)?
                            " || " + error :
                            Matrice210Application.visibleErrorsStr + error;
        }
    }

    protected static void writeToErrFile(String str) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String errStr = timestamp.toString() + ": " + str + "\n";
            BufferedWriter writer = new BufferedWriter(new FileWriter("/sdcard/theasis.err", true));
            PrintWriter out = new PrintWriter(writer);
            out.println(errStr);
            out.close();
        } catch (IOException e) {
            Log.v("ErrFileWrite", e.getMessage());
        }
    }

    protected static void writeToLogFile(String str) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String errStr = timestamp.toString() + ": " + str + "\n";
            BufferedWriter writer = new BufferedWriter(new FileWriter("/sdcard/theasis.log", true));
            PrintWriter out = new PrintWriter(writer);
            out.println(errStr);
            out.close();
        } catch (IOException e) {
            Log.v("LogFileWrite", e.getMessage());
        }
    }


}
